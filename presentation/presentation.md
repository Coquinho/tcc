Apresentação:
  - Motivação;
Conhecimentos Previos:
  - Qubit;
MO1QFA:
  - Definição;
  - Diferença MO1QFA e do FA:
    - Estado;
    - Função transição;
    - Computação;
Problema do módulo;
Problema da mesma quantidade de a's e b's;
Experimentação:
  - Arquitetura dos experimentos;
  - Mapeamento pra modelo circuital;
  - Mapeamento do problema do módulo para o modelo circuital;
  - Erro:
    + Resultados obtidos;
    + Função de erro;
    + Função de aproximação do erro;
    + Erro real;
Conclusão
Trabalhos futuros
