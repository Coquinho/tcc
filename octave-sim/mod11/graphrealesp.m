format long E
esp = load("esp.txt");
real = load("real.txt");
esp = 100-esp;
#beta = 8.493353608609491
#alpha = (100 -( 6.664502489697802+beta))/100
#esp = esp.*alpha+beta;
#esp = esp.*0.85.+8.5;
real = 100-real;
real = real';

real = real(2:2:end);

#vals = 0:11:1000;
#vals = [vals 3:11:1000];
vals = [3:11:1000];
vals = sort(vals);


esp = [];
for i = vals
  theta = i*2*pi/11;
  aux = cos(theta);
  esp = [esp aux*aux*100];
endfor
esp;

#esp = esp(2:2:end);
#real = real(2:2:end);

size(real);
size(esp);
size(vals);

erro = abs(esp-real);
[maxerro, i] = max(erro);
maxerro = maxerro
value = vals(i)
[minerro, i] = min(erro);
minerro = minerro
value = vals(i)

media = sum(erro)/size(erro)(1)
variancia = sum((erro-media).*(erro-media))/size(erro)(1)
desvio = sqrt(variancia)

erroReal = 5.329689393703506E-01 - media;
hf = figure ();
plot(vals,real, 'xb', vals,esp, 'or')

title ("Valores obtidos VS valores de aproximação");
txt = legend("Valores\naproximados", "Valores\nesperados");
legend boxoff
legend (txt, "location", "northeastoutside");
xlabel ("Tamanho da palavra");
ylabel ("Probabilidade de ser aceita");
set (txt, "fontsize", 20);
set(gca,  "fontsize", 20)

