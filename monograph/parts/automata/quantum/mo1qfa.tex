\section{\textit{Measure-Once Quantum Finite Automata}}
\label{sec::qaf}

Como visto na seção anterior o MO1QFA é o modelo mais simples de autômato
finito quântico e que possui a menor expressividade. Esse modelo foi proposto
por \textit{Moore C.} e \textit{Crutchfield J.} \cite{moore2000quantum} em
2000, com o intuito de adicionar os efeitos quânticos ao autômato finito, tais
como: transições dadas por transformações unitárias e a necessidade de se medir
o sistema para obter alguma informação. A sigla MO1QFA vêm de
\textit{measure-once 1-way quantum finite automata}, nome obtido dado algumas
características do seu funcionamento:
\begin{itemize}
    \item o estado quântico é medido uma única vez e sempre ao final da
        execução, por isto é chamado de \textit{measure-once};
    \item em comparação a outras versões de autômatos que podem mover o
        cabeçote de leitura para ambos os lados, o MO1QFA é chamado de
        \textit{1-way}, já que seu cabeçote de leitura se move em uma única
        direção, sempre em direção ao fim da palavra. No início da computação o
        cabeçote está no primeiro caractere da palavra e, a cada transição,
        avança um caractere até o fim da palavra.
\end{itemize}

\begin{ddef}
    Um MO1QFA é uma quíntupla $M = (Q,\Sigma,\delta,\ket{q_0},F)$ onde:
    \begin{description}
        \item[$Q = \{\ket{q_0}, \hdots, \ket{q_n}\}$] é um conjunto finito de
            estados - $Q$ é contido por um espaço de Hilbert;
        \item[$\Sigma = \{\sigma_0, \hdots, \sigma_k\}$] é um conjunto finito do
            alfabeto de entrada;
          \item[$\delta$] é um conjunto de matrizes unitárias, $U_{\sigma}$,
            que descreve as transições entre estados para cada símbolo $\sigma$
            do alfabeto de entrada;
        \item[$\ket{q_0}$] $\in Q$ e é o estado inicial do autômato;
        \item[$F$] $\subseteq Q$ é o conjunto de estados finais.
    \end{description}
    A computação de uma palavra $w$ é dada por:
    \begin{equation}
        f(w) = \norm{P_{acc}U_w\ket{q_0}}^2,
    \end{equation}
    onde $U_w$ é operar do caractere inicial ao final da palavra:
    \begin{equation}
        U = U_{w_{|w|-1}} \hdots U_{w_1}U_{w_0},
    \end{equation}
    e $P_{acc}$ é o operador de projeção conjunto de estados finais do
    autômato:
    \begin{equation}
        P_{acc} = \sum_{q_i\in F} \ketbra{q_i}{q_i}.
    \end{equation}
    f(w) representa a probabilidade de $M$ aceitar w.
\end{ddef}

Seguem agora alguns exemplos mostrando o funcionamento do MO1QFA e comparando-o
a como seria tal funcionamento nas versões clássicas. Dentro dos exemplos a
seguir para fins didáticos de obter a chance de uma palavra ser aceita, são
mostrados os valores do estado do sistema, os valores $\alpha$ e $\beta$. Em
uma execução normal não teríamos acesso a estes valores, somente as respostas
$0$ ou $1$.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Problema do módulo}
\label{mo1qfa:mod} \textit{Say} e \textit{Yakary{\i}lmaz} \cite{say2014quantum}
propuseram que, com MO1QFA podemos resolver problemas que possuem erro limitado
utilizando menos estados quando comparado à mesma solução em um autômato finito
clássico, e para demonstrar isso estudaram o problema do módulo de um número primo.

O reconhecimento com erro limitado possui 3 categorias\cite{BoundError}:
\begin{description}
    \item[\textit{zero-sided}] o reconhecimento nunca erra, porém em alguns
        casos, não consegue nem reconhecer nem rejeitar, e portanto retorna que
        não sabe;
    \item[\textit{one-sided} ou unilateral] o reconhecimento ou possui falsos
        positivos ou falsos negativos, nunca os dois;
    \item[\textit{two-sided} ou bilateral] o reconhecimento possui tanto falsos
        positivos quanto falsos negativos;
\end{description}

\textit{Say} e \textit{Yakary{\i}lmaz} \cite{say2014quantum} apresentaram um
MO-1QFA que reconhece a linguagem MOD$^p$ com erro limitado, permitindo falsos
positivos. A linguagem estudada é definida de modo similar ao feito na
\cref{mcc:dfa:ex:módulo}:
\begin{equation}
    \text{MOD}^p = \{a^{jp} | \text{ j é um inteiro não negativo}\}
\end{equation}
A linguagem é sobre o alfabeto unário $\Sigma = \{a\}$, e reconhece sequências
de $a$'s múltiplas de um primo $p$.

Um DFA para reconhecer MOD$^p$ foi apresentado na \cref{mcc:dfa:ex:módulo}, tal
autômato é exato, não possui erro, e possuiria $p$ estados.

No caso de um MO1QFA por outro lado, podemos nos ater a somente dois estados.
Para isso, podemos interpretar todas as possíveis possibilidades de
superposição de dois estados como na \cref{mo1qfa:mod:fig:poss}; o objetivo é,
a partir do estado inicial $\ket{0}$, dar voltas na esfera a cada $p$ $a$'s,
assim sempre que a palavra estiver em um $a$ múltiplo de $p$ o estado do
sistema será $\ket{0}$.

\begin{figure}[h]
\begin{center}
\begin{tikzpicture}
    \tikzmath{
        \raio = 1;
        \eixo = \raio + 0.25;
    }

    \draw (0,0) circle (\raio cm);
    \draw[thick,->] (-\eixo,0) node[left] {$-\ket{q_0} \leftrightarrow
        |w| \text{ mod}_p \equiv \frac{2p}{4} = \frac{p}{2}$} -- (\eixo,0) node[right] {$\ket{q_0} \leftrightarrow
        |w| \text{ mod}_p \equiv 0$};
    \draw[thick,->] (0,-\eixo) node[below] {$-\ket{q_1} \leftrightarrow
        |w| \text{ mod}_p \equiv \frac{3p}{4}$} -- (0,\eixo) node[above] {$\ket{q_1}
        \leftrightarrow |w| \text{ mod}_p \equiv \frac{p}{4}$};
\end{tikzpicture}
\end{center}
\caption{Conjunto de superposições possíveis com 2 estados. Onde $|w|$
    representa o tamanho da palavra $w$ computada.}
\label{mo1qfa:mod:fig:poss}
\end{figure}

Para tal, podemos utilizar a seguinte matriz de rotação:

\begin{equation}
A = U_{\theta = \frac{2\pi}{p}} =
\begin{bmatrix}
    cos\theta & -sin\theta\\
    sin\theta & cos\theta
\end{bmatrix},
\end{equation}
onde, com $\theta = \frac{2\pi}{p}$ faremos $p$ transições, saindo de $\ket{0}$
e voltando para $\ket{0}$, porém possibilitando falsos positivos. Nos casos em
que o estado do autômato se aproxima de $-\ket{q_0}$ é onde ocorre a maior
chance de erro. A chance de um falso positivo é vinculada ao primo em questão e
é dada por:
\begin{equation}
\label{mo1qfa:mod:erroeq}
    \cos^2\left(\frac{\pi}{p}\right) = 1 - \sin^2\left(\frac{\pi}{p}\right).
\end{equation}

Analisando a função de variação do erro com relação ao primo utilizado
(\cref{mo1qfa:mod:fig:erro}), podemos ver que quanto maior é o primo $p$ mais
próximo de 1 é o erro máximo, ou seja, quanto maior o primo, maior a frequência
de falsos positivos e maior é a probabilidade deles serem aceitos. Além disso,
é possível ver que o erro associado cresce de maneira de maneira acelerada,
atingindo mais de 80\% de erro já com primos maiores que 7, o que pode ser um
empecilho para casos que não aceitem erro.

\begin{figure}[h]
\begin{center}
\includegraphics[height=6cm]{img/quantum-automata/modpError.png}
\caption{Variação do erro com relação ao primo utilizado. No eixo $x$ o tamanho
do primo $p$, e no eixo $y$ o valor de erro máximo.}
\label{mo1qfa:mod:fig:erro}
\end{center}
\end{figure}

\subsubsection*{Exemplo - MOD$^7$}
Para MOD$^7$ a linguagem é definida por:
\begin{equation}
    \text{MOD}^{7} = \{ a^{7j}| j\text{ é um inteiro não negativo}\}
\end{equation}

Um \textbf{DFA}, seguindo a notação de Dirac, para MOD$^7$ pode ser definido por
$D = (Q,\Sigma,\delta,q_0,F)$:
\begin{description}
    \item[Q] é \{$\ket{0}$,$\ket{1}$,$\hdots$,$\ket{6}$\};
    \item[$\Sigma$] é \{a\};
    \item[$\delta$] é dado pela seguinte matriz de dimensões $7 \times 7$
        \begin{equation}
            A = \sum_{i=1}^{7} \ketbra{i \% 7}{i - 1}
        \end{equation}
        , onde "\%" é a operação módulo;
    \item[$q_0$] é $\ket{0}$;
    \item[F] é $\{\ket{0}\}$.
\end{description}

A computação de $w =aaa$ seria dada por:
\begin{equation}
\begin{split}
    f(w) = \norm{P_{acc}U_w\ket{q_0}}^2 = \norm{\ketbra{0}{0}AAA\ket{0}}^2 \\
    = \norm{\ketbra{0}{0}AA\ket{1}}^2 = \norm{\ketbra{0}{0}A\ket{2}}^2 \\
    = \norm{\ketbra{0}{0}\ket{3}}^2 = \norm{0}^2 = 0,
\end{split}
\end{equation}
portanto $w =aaa$ é rejeitada. De fato, podemos verificar que $3 \% 7 \equiv 3$
e não $0$, isso se dá pois, nesta palavra, $j$ não é um inteiro não negativo.

A computação de $w = a^7$ seria dada por:
\begin{equation}
\begin{split}
    f(w) = \norm{P_{acc}U_w\ket{q_0}}^2 = \norm{\ketbra{0}{0}A^7\ket{0}}^2 \\
    = \norm{\ketbra{0}{0}A^6\ket{1}}^2 = \norm{\ketbra{0}{0}A^5\ket{2}}^2 \\
    = \norm{\ketbra{0}{0}A^4\ket{3}}^2 = \norm{\ketbra{0}{0}A^3\ket{4}}^2 \\
    = \norm{\ketbra{0}{0}A^2\ket{5}}^2 = \norm{\ketbra{0}{0}A\ket{6}}^2 \\
    = \norm{\ketbra{0}{0}\ket{0}}^2 = \norm{\ket{0}}^2 = \sqrt{1}^2 = 1,
\end{split}
\end{equation}
portanto $w =a^7$ é aceita como esperado,já que $3 \% 7 \equiv 0$.

A computação de $w = a^{16}$ seria dada por:
\begin{equation}
\begin{split}
    f(w) = \norm{P_{acc}U_w\ket{q_0}}^2 = \norm{\ketbra{0}{0}A^{16}\ket{0}}^2 \\
    = \norm{\ketbra{0}{0}A^{15}\ket{1}}^2
    = \norm{\ketbra{0}{0}A^{14}\ket{2}}^2 \\
    \vdots\\
    = \norm{\ketbra{0}{0}A^4\ket{5}}^2 = \norm{\ketbra{0}{0}A^3\ket{6}}^2 \\
    = \norm{\ketbra{0}{0}A^2\ket{0}}^2 = \norm{\ketbra{0}{0}A\ket{1}}^2 \\
    = \norm{\ketbra{0}{0}\ket{2}}^2 = \norm{0}^2 = 0,
\end{split}
\end{equation}
portanto $w =a^{16}$ é rejeitada. De fato, podemos verificar que $16 \% 7 \equiv
2$ e não $0$, isso se dá pois, nesta palavra, $j$ não é um inteiro não
negativo.

Por sua vez, um \textbf{QFA} para MOD$^7$ é definido por
$M = (Q,\Sigma,\delta,q,F)$:
\begin{description}
    \item[Q] é \{$\ket{0}$,$\ket{1}$\} | $\ket{0}$ e $\ket{1}$ são
        ortonormais;
    \item[$\Sigma$] é \{$a$\};
    \item[$\delta$] é dado pela matriz:
        \begin{equation}
          A = U_{\theta} =
            \begin{bmatrix}
                \cos\theta & -\sin\theta\\
                \sin\theta & \cos\theta
              \end{bmatrix},
        \end{equation}
        onde $\theta = \frac{2\pi}{7}$;
    \item[q] é $\ket{0}$;
    \item[F] é $\{\ket{0}\}$.

\end{description}

Outra informação útil nesse caso, é o cálculo da chance de um falso positivo:
\begin{equation}
\label{mo1qfa:mod:eq:erro}
    cos^2\left(\frac{\pi}{p}\right) = cos^2\left(\frac{\pi}{7}\right) \approx 0,81174,
\end{equation}
ou seja, as palavras que possuem a maior taxa de falso positivo terão 81,17\%
de chance de serem aceitas, mesmo não pertencendo a linguagem.

A computação de $w =aaa$ seria dada por:
\begin{equation}
\begin{split}
    f(w) = \norm{P_{acc}U_w\ket{q_0}}^2 = \norm{\ketbra{0}{0}AAA\ket{0}}^2 \\
    = \norm{\ketbra{0}{0}AA(0,62349\ket{0} + 0,78183\ket{1})}^2 \\
    = \norm{\ketbra{0}{0}A(-0,22252\ket{0} + 0,97493\ket{1})}^2 \\
    = \norm{\ketbra{0}{0}(-0,90097\ket{0} + 0,43388\ket{1})}^2 \\
    = \norm{-0,90097\ket{0}}^2 = \sqrt(0,81174)^2 = 0,81174,
\end{split}
\end{equation}
portanto $w =aaa$ possui 81,17\% de chance de ser aceita. De fato, podemos
verificar que $3 \% 7 \equiv 3$ e não $0$, e portanto deveria ter uma
probabilidade de ser aceita menor que $1$ e no máximo 81,17\%, como calculado
anteriormente na \cref{mo1qfa:mod:eq:erro}.

A computação de $w = a^7$ seria dada por:
\begin{equation}
\begin{split}
    f(w) = \norm{P_{acc}U_w\ket{q_0}}^2 = \norm{\ketbra{0}{0}A^7\ket{0}}^2 \\
    = \norm{\ketbra{0}{0}A^6(0,62349\ket{0} + 0,78183\ket{1})}^2 \\
    = \norm{\ketbra{0}{0}A^5(-0,22252\ket{0} + 0,97493\ket{1})}^2 \\
    = \norm{\ketbra{0}{0}A^4(-0,90097\ket{0} + 0,43388\ket{1})}^2 \\
    = \norm{\ketbra{0}{0}A^3(-0,90097\ket{0} - 0,43388\ket{1})}^2 \\
    = \norm{\ketbra{0}{0}A^2(-0,22252\ket{0} - 0,97493\ket{1})}^2 \\
    = \norm{\ketbra{0}{0}A(0,62349\ket{0} - 0,78183\ket{1})}^2 \\
    = \norm{\ketbra{0}{0}\ket{0}}^2 = \norm{\ket{0}}^2 = \sqrt{1}^2 = 1,
\end{split}
\end{equation}
portanto $w =a^7$ é aceita com 100\% de chance como esperado,já que $7 \% 7
\equiv 0$.

A computação de $w = a^{16}$ seria dada por:
\begin{equation}
\begin{split}
    f(w) = \norm{P_{acc}U_w\ket{q_0}}^2 = \norm{\ketbra{0}{0}A^{16}\ket{0}}^2 \\
    = \norm{\ketbra{0}{0}A^{15}(0,62349\ket{0} + 0,78183\ket{1})}^2 \\
    = \norm{\ketbra{0}{0}A^{14}(-0,22252\ket{0} + 0,97493\ket{1})}^2 \\
    = \norm{\ketbra{0}{0}A^{13}(-0,90097\ket{0} + 0,43388\ket{1})}^2 \\
    = \norm{\ketbra{0}{0}A^{12}(-0,90097\ket{0} - 0,43388\ket{1})}^2 \\
    = \norm{\ketbra{0}{0}A^{11}(-0,22252\ket{0} - 0,97493\ket{1})}^2 \\
    \vdots \\
    = \norm{\ketbra{0}{0}A^{3}(0,62349\ket{0} - 0,78183\ket{1})}^2 \\
    = \norm{\ketbra{0}{0}A^{2}\ket{0}}^2 \\
    = \norm{\ketbra{0}{0}A(0,62349\ket{0} + 0,78183\ket{1})}^2 \\
    = \norm{\ketbra{0}{0}(-0,22252\ket{0} + 0,97493\ket{1})}^2 \\
    = \norm{-0,22252\ket{0}}^2 = \sqrt{0,049516}^2 = 0,049516,
\end{split}
\end{equation}
portanto $w =a^{16}$ possui 4,95\% de probabilidade de ser aceita. De fato,
podemos verificar que $16 \% 7 \equiv 2$ e não $0$, e portanto deveria ter uma
probabilidade de ser aceita menor que $1$ e no máximo 81,17\%, como calculado
anteriormente na \cref{mo1qfa:mod:eq:erro}.

\subsubsection*{Observação}

Foi observado que além de reconhecer MOD$^p$ este autômato também é capaz de
reconhecer MOD$^n$ onde n não é um primo ao utilizar $\theta = \frac{\pi}{n}$:

\begin{equation}
A = U_{\theta = \frac{\pi}{n}} =
\begin{bmatrix}
    cos\theta & -sin\theta\\
    sin\theta & cos\theta
\end{bmatrix},
\end{equation}
isso ocorre pois, a alteração do vetor estado ocorre seguindo duas senóides
opostas, como na \cref{mo1qfa:mod:fig:vetorEstado:mod13}.

\begin{figure}[h]
\begin{center}
\includegraphics[height=6cm]{img/quantum-automata/vetorEstadoMod13.png}
\caption{Variação do vetor estado ao ler 13 caracteres no MO1QFA que reconhece
    MOD$^{13}$. Em vermelho se encontra o valor de amplitude do estado
    $\ket{0}$ e em azul o do $\ket{1}$.}
\label{mo1qfa:mod:fig:vetorEstado:mod13}
\end{center}
\end{figure}

Olhando a função completa, é possível observar que o período das senoides se
repete exatamente na metade do primo selecionado, ou seja, quando o resultado
do módulo $p$ do tamanho da entrada resulta em $\frac{p}{2}$. Assim, ao
substituir no ângulo $\theta$ o valor do primo $p$ por 2$n$ onde $n$ é o
natural que se quer o módulo, obtém-se o mapeamento da
\cref{mo1qfa:mod:fig:poss::modn}.
\begin{figure}[h]
\begin{center}
\begin{tikzpicture}
    \tikzmath{
        \raio = 1;
        \eixo = \raio + 0.25;
    }

    \draw (0,0) circle (\raio cm);
    \draw[thick,->] (-\eixo,0) node[left] {$-\ket{q_0} \leftrightarrow
        |w| \text{ mod}_n \equiv 0$} -- (\eixo,0) node[right] {$\ket{q_0} \leftrightarrow
        |w| \text{ mod}_n \equiv 0$};
    \draw[thick,->] (0,-\eixo) node[below] {$-\ket{q_1} \leftrightarrow
        |w| \text{ mod}_n \equiv \frac{n}{2}$} -- (0,\eixo) node[above] {$\ket{q_1}
        \leftrightarrow |w| \text{ mod}_n \equiv \frac{n}{2}$};
\end{tikzpicture}
\end{center}
\caption{Conjunto de superposições possíveis com 2 estados. Onde $|w|$
    representa o tamanho da palavra $w$ computada.}
\label{mo1qfa:mod:fig:poss::modn}
\end{figure}


Este modelo retira os picos observados nos valores congruentes a $\frac{n}{2}$
módulo $n$, como os existentes na \cref{mo1qfa:mod:fig:vetorEstado:mod13} .
Além disso, obtém-se a seguinte matriz de transição:
\begin{equation}
\begin{split}
\theta = \frac{2\pi}{p} = \frac{2\pi}{2n} = \frac{\pi}{n}, \\
A = U_{\theta = \frac{\pi}{n}} =
\begin{bmatrix}
    cos\theta & -sin\theta\\
    sin\theta & cos\theta
  \end{bmatrix}.
\end{split}
\end{equation}

Além disso, com essa alteração o valor do erro máximo será observado nos
valores que tendem a congruência à $0$ módulo $n$, resultando em:
\begin{equation}
  cos^2\left((n+1)\frac{\pi}{n}\right),
\end{equation}
que dada a natureza do cosseno, resulta na mesma função definida na
\cref{mo1qfa:mod:erroeq}.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\subsection{Problemas de promessa}

\textit{Say} e \textit{Yakary{\i}lmaz} \cite{say2014quantum} propuseram que o
MO-1QFA poderia resolver problemas de promessa com soluções exatas utilizando
menos estados que a solução em um autômato finito.

Um problema de promessa $P$ é definido por uma tupla de duas linguagens
$P=(P_{s}, P_{n})$ onde $P_{s}$ (resp.  $P_{n}$) é linguagem, consequentemente
o conjunto de palavras aceitas (resp. não aceitas). E qualquer outra palavra,
as palavras fora de $P_{s}\cup P_{n}$, não são importantes, ou seja, podem
tanto ser aceitas quanto rejeitadas. Tem-se também que em um problema de
promessa $P_{s}\cap P_{n} = \varnothing$.

Por sua vez, um problema de promessa é resolvido exatamente por um QFA (resp.
DFA) $M$ se: $M$ aceita com probabilidade 1(resp. aceita), toda palavra
pertencente a $P_{s}$; e $M$ aceita com probabilidade 0 (resp. rejeita), toda
palavra pertencente a $P_{n}$.

\textit{Say} e \textit{Yakary{\i}lmaz} \cite{say2014quantum} estudaram o
seguinte problema:
\begin{gather}
    P^{k} = (\text{EVEN}^{k}, \text{ODD}^{k}) \\
    \text{EVEN}^{k} = \{a^{j2^{k}} | \text{ j é um inteiro não negativo par}\}\\
    \text{ODD}^{k} = \{a^{j2^{k}} | \text{ j é um inteiro não negativo ímpar}\}
\end{gather}

Um DFA $M$ para resolver $P^{k}$ precisaria de $2^{k+1}$ estados
\cite{ambainis2012superiority}. Como, tanto no caso de estar em uma contagem
ímpar quanto em uma par, precisamos manter a contagem de quantos dos $2^k$
$a$'s já foram lidos, o funcionamento poderia ser, inicialmente, separado em
duas partes, a par e a ímpar, onde ambas fazem uma contagem de $a$'s. Para
resolver módulo de $2^{k}$ $a$'s, cada parte precisa contar de $0$ até $2^{k}$,
e portanto, cada parte precisa de $2^{k}+1$ estados.

Ao unir as duas partes podemos reutilizar o estado final da parte par (resp.
ímpar) como estado inicial da parte ímpar (resp. par), diminuindo dois estado,
totalizando, $2^{k+1}$ estados. Assim, $M$ difere quando está em uma contagem
par ou ímpar, e em ambos os casos, calcula módulo de $2^k$. Além disso, o estado
inicial e final de $M$ serão o estado inicial da parte par, assim, aceitando
somente nos casos em que $j$ é par.

Já um QFA $Q$ pode ser definido de maneira muito mais sucinta, com apenas dois
estados. Um estado para representar quando a contagem de $2^k$ $a$'s é par e um
para quando a contagem é ímpar. No estado $\ket{0}$ (resp. $\ket{1}$) temos uma
contagem par (resp. ímpar) de $2^k$ $a$'s até o momento.  Podemos analisar o
conjunto das possíveis superposições dos estados como um círculo onde cada
estado é um eixo (\cref{fig:poss}). Sabemos que a cada $2^{k}$ $a$'s alternamos
a contagem de $j$ entre par e ímpar, podemos aplicar essa operação através de
rotações de 90\textdegree. Com isso, a cada $2^k$ $a$'s alternamos entre os
estados da seguinte forma: $\ket{q_0}$, $\ket{q_1}$, $-\ket{q_0}$, $-\ket{q_1}$
e o ciclo se repete.

\begin{figure}[h]
\begin{center}
\begin{tikzpicture}
    \tikzmath{
        \raio = 1;
        \eixo = \raio + 0.25;
    }

    \draw (0,0) circle (\raio cm);
    \draw[thick,->] (-\eixo,0) node[left] {$-\ket{q_0} \leftrightarrow
        \text{ mod}_4 j \equiv 2$} -- (\eixo,0) node[right]
        {$\ket{q_0}\leftrightarrow\text{ mod}_4 j \equiv 0$};
    \draw[thick,->] (0,-\eixo) node[below] {$-\ket{q_1}\leftrightarrow
        \text{ mod}_4 j \equiv 3$} -- (0,\eixo) node[above]
        {$\ket{q_1}\leftrightarrow\text{ mod}_4 j \equiv 1$};
\end{tikzpicture}
\end{center}
\caption{Conjunto de superposições possíveis com 2 estados.}
\label{fig:poss}
\end{figure}


Para aplicar uma rotação de 90\textdegree $\left(\frac{\pi}{2}\right)$ a cada $2^{k}$
$a$'s, cada $a$ deve-se rotacionar $\theta = \frac{\pi}{2^{k+1}}$.
Como inicialmente não lemos nenhum $a$, e 0 é uma quantidade par de $2^k$
$a$'s,  o estado inicial será $\ket{q_0}$.

Para aplicar tais rotações temos a seguinte matriz, que será operador de
transição para único símbolo do alfabeto:
\begin{equation}
A = U_{\theta = \frac{\pi}{2^{k+1}}} =
\begin{bmatrix}
    cos\theta & -sin\theta\\
    sin\theta & cos\theta
\end{bmatrix}
\end{equation}

Em uma computação, a cada 4 grupos de $2^k$ $a$'s, completamos uma volta no círculo,
a cada $2^k$ $a$'s alterna-se entre os eixos: $\ket{q_0}$, $\ket{q_1}$, $-\ket{q_0}$,
$-\ket{q_1}$.
Desta forma, qualquer $P^k$ tem um reconhecimento exato por um QFA de dois
estados, uma quantia bem inferior quando comparada aos $2^{k+1}$ estados na
contrapartida clássica (DFA).

\subsubsection*{Exemplo - $P^3$}

Para $P^3$ temos:
\begin{equation}
\begin{split}
    P^{3} = (\text{EVEN}^{3}, \text{ODD}^{3}) \\
    \text{EVEN}^{3} = \{a^{j2^{3}} = a^{8j}
    | \text{ j é um inteiro não negativo par}\}\\
    \text{ODD}^{3} = \{a^{j2^{3}}  = a^{8j}
    | \text{ j é um inteiro não negativo ímpar}\}
\end{split}
\end{equation}

Um \textbf{DFA}, seguindo a notação de Dirac, para $P^3$ pode ser definido por
$D = (Q,\Sigma,\delta,q_0,F)$:
\begin{description}
    \item[Q] é \{$\ket{0}$,$\ket{1}$,$\hdots$,$\ket{15}$\};
    \item[$\Sigma$] é \{a\};
    \item[$\delta$] é dado pela seguinte matriz de dimensões $16 \times 16$
        \begin{equation}
            A = \sum_{i=1}^{16} \ketbra{i \% 16}{i - 1}
        \end{equation}
        , onde "\%"\ é a operação módulo;
    \item[$q_0$] é $\ket{0}$;
    \item[F] é $\{\ket{0}\}$.
\end{description}

A computação de $w =aaa$ seria dada por:
\begin{equation}
\begin{split}
    f(w) = \norm{P_{acc}U_w\ket{q_0}}^2 = \norm{\ketbra{0}{0}AAA\ket{0}}^2 \\
    = \norm{\ketbra{0}{0}AA\ket{1}}^2 = \norm{\ketbra{0}{0}A\ket{2}}^2 \\
    = \norm{\ketbra{0}{0}\ket{3}}^2 = \norm{0}^2 = 0
\end{split}
\end{equation}
portanto $w =aaa$ é rejeitada.

A computação de $w = a^8$ seria dada por:
\begin{equation}
\begin{split}
    f(w) = \norm{P_{acc}U_w\ket{q_0}}^2 = \norm{\ketbra{0}{0}A^8\ket{0}}^2 \\
    = \norm{\ketbra{0}{0}A^7\ket{1}}^2 = \norm{\ketbra{0}{0}A^6\ket{2}}^2 \\
    = \norm{\ketbra{0}{0}A^5\ket{3}}^2 = \norm{\ketbra{0}{0}A^4\ket{4}}^2 \\
    = \norm{\ketbra{0}{0}A^3\ket{5}}^2 = \norm{\ketbra{0}{0}A^2\ket{6}}^2 \\
    = \norm{\ketbra{0}{0}A\ket{7}}^2 = \norm{\ketbra{0}{0}\ket{8}}^2 \\
     = \norm{0}^2 = 0,
\end{split}
\end{equation}
portanto $w =a^8$ é rejeitada, o que faz sentido já que para $|w| = 8$ o valor
de $j$ em  $a^{8j}$ deve ser 1, um número ímpar.

A computação de $w = a^{16}$ seria dada por:
\begin{equation}
\begin{split}
    f(w) = \norm{P_{acc}U_w\ket{q_0}}^2 = \norm{\ketbra{0}{0}A^{16}\ket{0}}^2 \\
    = \norm{\ketbra{0}{0}A^{15}\ket{1}}^2 =
    \norm{\ketbra{0}{0}A^{14}\ket{2}}^2 \\
    = \norm{\ketbra{0}{0}A^{13}\ket{3}}^2 =
    \norm{\ketbra{0}{0}A^{12}\ket{4}}^2 \\
    \vdots\\
    = \norm{\ketbra{0}{0}A^3\ket{13}}^2 = \norm{\ketbra{0}{0}A^2\ket{14}}^2 \\
    = \norm{\ketbra{0}{0}A\ket{15}}^2 = \norm{\ketbra{0}{0}\ket{0}}^2 \\
     = \norm{1}^2 = 1,
\end{split}
\end{equation}
portanto $w =a^{16}$ é aceita, o que faz sentido já que para $|w| = 16$ o valor
de $j$ em  $a^{8j}$ deve ser 2, um número par.


Por sua vez, um \textbf{QFA} para $P^3$ é definido por
$M = (Q,\Sigma,\delta,q,F)$:
\begin{description}
    \item[Q] é \{$\ket{0}$,$\ket{1}$\} | $\ket{0}$ e $\ket{1}$ são
        ortonormais;
    \item[$\Sigma$] é \{$a$\};
    \item[$\delta$] é dado pela matriz:
        \begin{equation}
            A = U_{\theta = \frac{\pi}{2^{3+1}}} =
            U_{\theta = \frac{\pi}{16}} =
            \begin{bmatrix}
                cos\theta & -sin\theta\\
                sin\theta & cos\theta
            \end{bmatrix};
        \end{equation}

    \item[q] é $\ket{0}$;
    \item[F] é $\{\ket{0}\}$.

\end{description}

A computação de $w =aaa$ seria dada por:
\begin{equation}
\begin{split}
    f(w) = \norm{P_{acc}U_w\ket{q_0}}^2 = \norm{\ketbra{0}{0}AAA\ket{0}}^2 \\
    = \norm{\ketbra{0}{0}AA(0,98079\ket{0}+0,19509\ket{1})}^2\\
    = \norm{\ketbra{0}{0}A(0,92388\ket{0}+0,38268\ket{1})}^2\\
    = \norm{\ketbra{0}{0}(0,83147\ket{0}+0,55557\ket{1})}^2\\
    = \norm{(0,83147\ket{0})}^2 = \sqrt{0,69134}^2 = 0,69134,
\end{split}
\end{equation}
portanto $w =aaa$ tem $69,13\%$ de chance de ser aceita, lembrando que essa
palavra não pertence nem a EVEN$^3$ nem a ODD$^3$ então ela ser ou não aceita é
irrelevante, o importante são as respostas para os membros de EVEN$^3$ e
ODD$^3$.

A computação de $w = a^8$ seria dada por:
\begin{equation}
\begin{split}
    f(w) = \norm{P_{acc}U_w\ket{q_0}}^2 = \norm{\ketbra{0}{0}A^8\ket{0}}^2 \\
    = \norm{\ketbra{0}{0}A^7(0,98079\ket{0}+0,19509\ket{1})}^2\\
    = \norm{\ketbra{0}{0}A^6(0,92388\ket{0}+0,38268\ket{1})}^2\\
    = \norm{\ketbra{0}{0}A^5(0,83147\ket{0}+0,55557\ket{1})}^2\\
    = \norm{\ketbra{0}{0}A^4(0,70711\ket{0}+0,70711\ket{1})}^2\\
    = \norm{\ketbra{0}{0}A^3(0,55557\ket{0}+0,83147\ket{1})}^2\\
    = \norm{\ketbra{0}{0}A^2(0,38268\ket{0}+0,92388\ket{1})}^2\\
    = \norm{\ketbra{0}{0}A(0,19509\ket{0}+0,98079\ket{1})}^2\\
    = \norm{\ketbra{0}{0}\ket{1}}^2
    = \norm{0}^2 = 0,
\end{split}
\end{equation}
portanto $w =a^8$ tem probabilidade 0 de ser aceita. O que faz bastante sentido
já que, neste caso, $j$ é $1$, um valor ímpar.

A computação de $w = a^{16}$ seria dada por:
\begin{equation}
\begin{split}
    f(w) = \norm{P_{acc}U_w\ket{q_0}}^2 = \norm{\ketbra{0}{0}A^{16}\ket{0}}^2 \\
    \vdots\\
    = \norm{\ketbra{0}{0}A^{12}\ket{1}}^2 \\
    \vdots\\
    = \norm{\ketbra{0}{0}A^{8}(-\ket{0})}^2 \\
    \vdots\\
    = \norm{\ketbra{0}{0}A^{4}(-\ket{1})}^2 \\
    \vdots\\
    = \norm{\ketbra{0}{0}-\ket{0}}^2 \\
    = \norm{-\ket{0}}^2 = \sqrt{1}^2 = 1,
\end{split}
\end{equation}
portanto $w =a^{16}$ é aceita. O que faz bastante sentido já que, neste caso,
$j$ é $2$, um valor par. Interessante ressaltar que, ao tomar a medida, o
estado do sistema será $-\ket{0}$, porém essa fase não faz diferença para a
medida.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\subsection{Número de $a$'s diferente do número de $b$'s}
\textit{Say} e \textit{Yakary{\i}lmaz} \cite{say2014quantum} propuseram que,
com MO1QFA não determinístico podemos resolver alguns problemas da classe das
linguagens livres de contexto (o conjunto de linguagens reconhecíveis por
autômatos de pilha não determinísticos\cite{sipse2012introduction}), o que é
impossível com autômatos finitos clássicos\cite{sipse2012introduction}, para
isso eles estudaram a linguagem sobre o alfabeto $\Sigma = \{a,b\}$, ou seja,
palavras constituídas de $a$'s e $b$'s em qualquer ordem, onde a quantidade de
$a$'s é diferente da quantidade de $b$'s. Tal linguagem é definida formalmente
da seguinte forma:
\begin{equation}
    L = \{(a+b)^*\mid \#a's \neq \#b's\}.
\end{equation}

Um QFA é dito não determinístico quando reconhece com ponto de corte 0 todas as
palavras da linguagem, ou seja, com probabilidade maior que 0 reconhece as
palavras da linguagem, e com probabilidade igual a 0 reconhece as palavras que
não pertencem a linguagem.

Na \cref{fig:ap:diagrama:adifb} é mostrado um autômato de Pilha(PFA) com 4
estados que reconhece $L$.

Por sua vez, é possível definir um MO1QFA para reconhecer essa linguagem com 2
estados de maneira similar aos problemas apresentados anteriormente. Esse
autômato tem $\ket{0}$ como estado inicial e $\ket{1}$ como estado final. A
ideia neste autômato é utilizarmos do fato que o conjunto de possibilidades
formam um círculo, e o fato de que um círculo possui infinitos pontos. Podemos
definir rotações no sentido horário para os $a$'s e anti-horário para os $b$'s,
assim, durante a computação cada ponto do círculo indica quantos $a$'s ou $b$'s
faltam para as quantidades ficarem iguais. Caso o número de $a$'s e $b$'s sejam
iguais, o estado do autômato será igual ao estado inicial e, portanto, o
resultado da medida será $\ket{0}$ com 100\% de chance. Caso contrário, a
probabilidade do resultado da medida ser $\ket{1}$ é maior que 0\%.

Para executar tais rotações é necessário que o ângulo de rotação seja um
múltiplo irracional de $\pi$, aqui será utilizado $\sqrt{2}\pi$. Dada a
seguinte matriz de rotação, com $\theta=\sqrt{2}\pi$ é feita uma rotação no
sentido anti-horário e com $\theta=-\sqrt{2}\pi$ no sentido horário:
\begin{equation}
\begin{split}
U_\theta =
\begin{bmatrix}
    cos\theta & -sin\theta\\
    sin\theta & cos\theta
\end{bmatrix},
\end{split}
\end{equation}

Um MO1QFA para $L = \{(a+b)^*\mid \#a's \neq \#b's\}$ pode ser definido por $M =
\left(Q,\Sigma,\delta,q,F\right)$:

\begin{description}
    \item[Q] é \{$\ket{0}$,$\ket{1}$\} | $\ket{0}$ e $\ket{1}$ são
        ortonormais;
    \item[$\Sigma$] é $\{a,b\}$;
    \item[$\delta$] é dado pela matriz:
        \begin{equation}
        \begin{split}
        A = U_{\theta = -\sqrt{2}\pi} =
        \begin{bmatrix}
            cos\theta & -sin\theta\\
            sin\theta & cos\theta
        \end{bmatrix},\\
        B = U_{\theta = \sqrt{2}\pi} =
        \begin{bmatrix}
            cos\theta & -sin\theta\\
            sin\theta & cos\theta
        \end{bmatrix};
        \end{split}
        \end{equation}

    \item[q] é $\ket{0}$;
    \item[F] é $\{\ket{1}\}$.

\end{description}

A computação de $w=aaaba$ seria dada por:
\begin{equation}
\begin{split}
    f(w) = \norm{P_{acc}U_w\ket{q_0}}^2 = \norm{\ketbra{1}{1}AAABA\ket{0}}^2&\\
    = \norm{\ketbra{1}{1}AAAB(-0,26626\ket{0}+0,96390\ket{1})}^2&\\
    = \norm{\ketbra{1}{1}AAA\ket{0}}^2&\\
    = \norm{\ketbra{1}{1}AA(-0,26626\ket{0}+0,96390\ket{1})}^2&\\
    = \norm{\ketbra{1}{1}A(-0,85822\ket{0}-0,51329\ket{1})}^2&\\
    = \norm{\ketbra{1}{1}(0,72326\ket{0}-0,69057\ket{1})}^2&\\
    = \norm{-0,69057\ket{1}}^2
    = \sqrt{0,47689}^2 = 0,47689,
\end{split}
\end{equation}
portanto $w =aaaba$ tem $47,68\%$ de chance de ser aceita. Já que $w$ possui
uma quantidade diferente de $a$'s e $b$'s, a probabilidade de $w$ ser aceita
é maior que 0\% como o esperado.

A computação de $w=aabb$ seria dada por:
\begin{equation}
\begin{split}
    f(w) = \norm{P_{acc}U_w\ket{q_0}}^2 = \norm{\ketbra{1}{1}AABB\ket{0}}^2&\\
    = \norm{\ketbra{1}{1}AAB(-0,26626\ket{0}-0,96390\ket{1})}^2&\\
    = \norm{\ketbra{1}{1}AA(-0,85822\ket{0}+0,51329\ket{1})}^2&\\
    = \norm{\ketbra{1}{1}A(-0,26626\ket{0}-0,96390\ket{1})}^2&\\
    = \norm{\ketbra{1}{1}\ket{0}}^2
    = \norm{0}^2 = 0,
\end{split}
\end{equation}
portanto $w =aabb$ tem $0\%$ de chance de ser aceita como o esperado, já que
$w$ possui uma quantidade igual de $a$'s e $b$'s, e portanto não pertence a
linguagem.

A computação de $w=abbaba$ seria dada por:
\begin{equation}
\begin{split}
    f(w) = \norm{P_{acc}U_w\ket{q_0}}^2 = \norm{\ketbra{1}{1}ABBABA\ket{0}}^2&\\
    = \norm{\ketbra{1}{1}ABBAB(-0,26626\ket{0}+0,96390\ket{1})}^2&\\
    = \norm{\ketbra{1}{1}ABBA\ket{0}}^2&\\
    = \norm{\ketbra{1}{1}ABB(-0,26626\ket{0}+0,96390\ket{1})}^2&\\
    = \norm{\ketbra{1}{1}AB\ket{0}}^2&\\
    = \norm{\ketbra{1}{1}A(-0,26626\ket{0}-0,96390\ket{1})}^2&\\
    = \norm{\ketbra{1}{1}\ket{0}}^2&\\
    = \norm{0}^2 = 0,
\end{split}
\end{equation}
portanto $w=abbaba$ tem $0\%$ de chance de ser aceita como o esperado, já que
$w$ possui uma quantidade igual de $a$'s e $b$'s, e portanto não pertence a
linguagem.

\subsubsection*{Observação}

O autômato apresentado pode ser facilmente alterado para reconhecimento de:

\begin{equation}
    L = \{(a+b)^*| \#a's = \#b's\},
\end{equation}
ou seja, uma linguagem que contém as palavras que possuem quantidades de
ocorrências de $a$'s iguais às de $b$'s. A alteração necessária é que o estado
inicial e final do autômato seja $\ket{0}$. Porém a alteração proposta
possibilita falsos positivos. Isso ocorre pois, quando as quantidades de $a$'s
e $b$'s são diferentes, existem somente dois pontos do círculo($\pm\ket{1}$)
onde não existe chance alguma do estado colapsar em $0$.

