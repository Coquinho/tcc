\section{Álgebra Linear para Computação Quântica}
\label{sec:al}

A álgebra linear tem suma importância na computação quântica. Dessa forma, nesta
seção será feita uma breve revisão da álgebra linear e algumas propriedades
utilizadas nos estudos em computação quântica e, consequentemente, no estudo dos
modelos computacionais que serão apresentados. Além disso, será apresentada a
notação \textit{Dirac}, a notação utilizada na mecânica quântica para descrever
os estados quânticos.

Em geral o interesse da computação quântica é voltado a espaços vetoriais de
dimensão finita sobre os números complexos($\mathbb{C}$), desta forma, a
revisão apresentada aqui se vê sobre este corpo.

A referência deste capítulo é a segunda seção de \textit{Nielsen} e
\textit{Chuang}\cite{nielsen2010quantum}, onde é abordada a álgebra linear para
computação quântica. Aqui alguns desses tópicos são revisados, entre eles:
espaço vetorial (\cref{al:espaçoVetorial}), base e independência linear
(\cref{al:il}), algumas operações relevantes referentes aos espaços vetoriais
(\cref{al:op}), transformações lineares (\cref{al:tl}), alguns operadores
lineares relevantes (\cref{al:operadores}) e produto tensorial(\cref{al:pt}).

%##############################################################################

\subsection{Espaço Vetorial}
\label{al:espaçoVetorial}

Na notação de \textit{Dirac}, também conhecida como notação \textit{braket}, um
estado quântico é um vetor coluna e é representado dentro de um \textit{ket}
$\ket{}$.  Então dado um estado quântico $\psi$ pertencente à $\mathbb{C}^n$
(vetores de dimensão $n$ composto por elementos complexos) temos:

\begin{equation}
    \ket{\psi} =
    (\psi_0\text{ , }\psi_{1}\text{ , }\ldots\text{ , }\psi_{n-1}) =
    \mqty[\psi_0 \\ \psi_{1} \\ \vdots \\ \psi_{n-1}].
\end{equation}

Um espaço vetorial $V$ sobre $\mathbb{C}^n$ é um conjunto de vetores de dimensão $n$ compostos por elementos complexos e equipado com as operações
indicadas abaixo. As operações a seguir já demonstram parte da notação de
\textit{Dirac}.

Para $\ket{\psi}$, $\ket{\zeta}$, $\ket{\varphi} \in \mathbb{C}^n$ e
$\alpha$, $\beta \in \mathbb{C}$ temos:

\begin{description}
  \item[soma entre vetores]
        \begin{equation}
            \ket{\psi} + \ket{\varphi} = \ket{\varphi} + \ket{\psi} =
            \mqty[\psi_0+\varphi_0 \\ \psi_{1}+\varphi_{1} \\ \vdots \\
                \psi_{n-1}+\varphi_{n-1}];
        \end{equation}
        \begin{equation}
            (\ket{\psi} + \ket{\varphi}) + \ket{\zeta} = \ket{\psi} +
            (\ket{\varphi} + \ket{\zeta});
        \end{equation}
        \begin{equation}
            \ket{\psi} + 0 = \mqty[\psi_0+0 \\ \psi_{1}+0 \\ \vdots \\
            \psi_{n-1}+0] = \ket{\psi};
        \end{equation}
        \begin{equation}
            \ket{\psi} -\ket{\psi} = \mqty[\psi_0-\psi_0 \\ \psi_{1}-\psi_{1}
            \\ \vdots \\ \psi_{n-1}-\psi_{n-1}] = 0.
        \end{equation}


      \item[multiplicação de vetor por escalar]
        \begin{equation}
            \alpha\ket{\psi} =
            \mqty[\alpha\psi_0 \\ \alpha\psi_{1} \\ \vdots \\ \alpha\psi_{n-1}];
        \end{equation}
        \begin{equation}
            (\alpha  \beta)  \ket{\psi} = \alpha  (\beta \ket{\psi});
        \end{equation}
        \begin{equation}
            \alpha(\ket{\psi}+\ket{\varphi}) =
            \alpha\ket{\psi} + \alpha\ket{\varphi};
        \end{equation}
        \begin{equation}
            (\alpha+\beta)\ket{\psi} = \alpha\ket{\psi}+\beta\ket{\psi}.
        \end{equation}
\end{description}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\subsection{Base Computacional e Independência Linear}
\label{al:il}
Um espaço vetorial pode ser gerado a partir da combinação linear de um conjunto
de vetores, este conjunto é chamado de base do espaço.

Uma base muito utilizada neste trabalho é a base computacional. Os vetores da
base computacional de dimensão $n$ são:
\begin{equation}
\ket{0} = \mqty[1\\0\\0\\\vdots\\0],
\ket{1} = \mqty[0\\1\\0\\\vdots\\0],
\ket{2} = \mqty[0\\0\\1\\\vdots\\0],
\hdots,
\ket{n-2} = \mqty[0\\\vdots\\0\\1\\0],
\ket{n-1} = \mqty[0\\\vdots\\0\\0\\1].
\end{equation}
Todos os vetores pertencentes a um espaço vetorial podem ser gerados a partir
de uma combinação linear dos vetores de sua base. Desta forma, dado um estado
$\psi$ que pertence a base computacional e $\alpha_{0},\alpha_{1},\hdots,
\alpha_{n}\in \mathbb{C}$, $\ket{\psi}$ pode ser descrito como:
\begin{equation}
    \ket{\psi} = \alpha_{0}\ket{0} + \alpha_{1}\ket{1} + \hdots
    +\alpha_{n}\ket{n}.
\end{equation}
\subsubsection*{Espaço de Hilbert}
Na computação quântica, em geral, trabalhamos sobre espaços vetoriais
chamados de espaço de Hilbert, que são espaços vetoriais munidos com a
operação de produto interno (\cref{al:op:pi}) e com a propriedade de completude.
Como, ao trabalhar em sistemas quânticos, utilizamos espaços de Hilbert com
dimensões finitas, a propriedade de completude é automática.

%###############################################################################

\subsection{Operações relevantes}
\label{al:op}
Nesta seção, é apresentado um conjunto de operações relevantes aos estudos de
espaços vetoriais.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\subsubsection*{Produto Interno}
\label{al:op:pi}
Se tratando de espaços reais, o produto interno correlaciona o tamanho de dois vetores ao ângulo entre eles,
retornando um escalar:
\begin{equation}
    (\ket{\psi},\ket{\varphi}) =
    \cos\theta \norm{\ket{\psi}}\norm{\ket{\varphi}} ,
\end{equation}
onde $\theta$ é o ângulo entre $\ket{\psi}$ e $\ket{\varphi}$ a operação norma
($\norm{}$) será definida mais adiante nessa seção.
Se tratando de números complexos, dada a dificuldade de pensar no produto
interno utilizando o ângulo entre vetores, se costuma utilizar um conceito
diferente, apresentado na \cref{al:op:pi:cn}.
Inicialmente é importante saber que por simplicidade, ao utilizamos a notação
de \textit{Dirac}, nos referimos ao produto interno entre $\ket{\psi}$ e
$\ket{\varphi}$, denotado anteriormente por $(\ket{\psi},\ket{\varphi})$,  por
um \textit{braket} entre os vetores: $\braket{\psi}{\varphi}$. Tal
representação leva a notação de \textit{Dirac} a também ser referenciada como
notação \textit{braket}.  O \textit{bra} de $\psi$ , denotado por $\bra{\psi}$,
é o vetor dual do \textit{ket} $\ket{\psi}$, definido como:

\begin{equation}
    \bra{\psi} = \ket{\psi}^{\dagger} =
    \mqty[\psi_0 \\ \vdots \\ \psi_{n-1}]^{\dagger} =
    \begin{bmatrix}
        \psi_0^{*} & \hdots &  \psi_{n-1}^{*},
      \end{bmatrix},
\end{equation}
onde a operação $^{*}$ denota o conjugado de um número complexo, e $^{\dagger}$
(adaga) o conjugado transposto de um vetor.


O produto interno em $\mathbb{C}^{n}$ também pode ser definido como o produto
de um \textit{bra} por um \textit{ket}:
\begin{equation}
  \label{al:op:pi:cn}
    (\ket{\psi},\ket{\varphi}) = \braket{\psi}{\varphi} =
    \begin{bmatrix}
        \psi_0^{*} & \psi_{1}^{*} & \hdots & \psi_{n-1}^{*}
    \end{bmatrix}
    \mqty[\varphi_0 \\ \varphi_{1} \\ \hdots \\ \varphi_{n-1}] =
    \sum_{i=0}^{n-1} \psi_{i}^* \varphi_{i}.
\end{equation}
Dessa forma, o produto interno $\braket{\psi}{\varphi}$ é o produto de
$\ket{\psi}^\dagger$ e $\ket{\varphi}$.

Além disso, dado um espaço vetorial $\mathbb{C}^{n}$, $\ket{\varphi}$,
$\ket{\psi}$ e $\ket{\zeta}\in\mathbb{C}^{n}$ e $\alpha\in\mathbb{C}$,  o
produto interno possui as seguintes propriedades:
\begin{description}
    \item[Simetria Hermitiana]
        \begin{equation}
            \braket{\varphi}{\psi} = \braket{\psi}{\varphi}^{};
        \end{equation}

    \item[Linearidade]
        \begin{equation}
            \braket{\varphi+\zeta}{\psi} = \braket{\varphi}{\psi} +
            \braket{\zeta}{\psi};
        \end{equation}

    \item[Associatividade]
        \begin{equation}
            \braket{\alpha\varphi}{\psi} = \alpha\braket{\varphi}{\psi};
        \end{equation}

    \item[Positividade]
        \[\braket{\varphi}{\varphi} \geq 0,\]
        \begin{equation}
            \braket{\varphi}{\varphi} = 0 \Leftrightarrow \varphi = 0;
        \end{equation}
        onde 0 é o vetor nulo.
\end{description}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\subsubsection*{Norma}
\label{al:operacoes:norma}

A norma de um vetor é uma operação que retorna um escalar que representa seu
tamanho, ela é definida por:
\begin{equation}
    \norm{\ket{\varphi}} = \sqrt{\braket{\varphi}{\varphi}} \geq 0.
\end{equation}

Além disso, existe o conceito de um vetor normalizado, ou normal, isto é, um
vetor com tamanho 1. Para normalizar um vetor, ou seja, manter sua direção no
espaço vetorial, porém deixar seu tamanho igual a 1, podemos dividir o
vetor por sua norma.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\subsubsection*{Ortogonalidade}

Dois vetores são ditos ortogonais quando o produto interno deles é nulo.
\begin{equation}
  \braket{\varphi}{\psi} = 0 \Leftrightarrow  \ket{\varphi} \perp \ket{\psi}.
\end{equation}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\subsubsection*{Ortonormalidade}

Um conjunto de vetores é dito ortonormal, quando todos os seus vetores são
normais, e ortogonais dois a dois. Desta forma, com relação ao produto interno
de quaisquer $\ket{\varphi}$ e $\ket{\psi}$ pertencentes ao conjunto temos que:

\begin{equation}
    \braket{\varphi}{\psi} =
    \begin{cases}
        1\text{, se $\ket{\varphi} = \ket{\psi}$} \\
        0\text{, se $\ket{\varphi} \neq \ket{\psi}$}
    \end{cases}.
\end{equation}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\subsubsection*{Produto Exterior}
Como, pela notação de \textit{Dirac}, $\bra{\psi}$ é um vetor linha, podemos
definir o produto exterior de $\ket{\varphi}$ e $\ket{\psi}$ como a
multiplicação matricial $\ketbra{\varphi}{\psi}$:

\begin{equation}
\begin{split}
    & \ketbra{\varphi}{\psi} =
    \mqty[\varphi_0\\\varphi_{1}\\\vdots\\\varphi_{n-1}]
    \begin{bmatrix}
        \psi_0^{} & \psi_{1}^{} & \hdots & \psi_{n-1}^{}
    \end{bmatrix}
    \\ = &
    \begin{bmatrix}
        \varphi_0\psi_0^{} & \varphi_0\psi_1^{} & \hdots &
        \varphi_0\psi_{n-1}^{} & \\
        \varphi_1\psi_0^{} & \varphi_1\psi_1^{} & \hdots &
        \varphi_1\psi_{n-1}^{} & \\
        \vdots & \vdots & \ddots & \vdots \\
        \varphi_{n-1}\psi_{0}^{} & \varphi_{n-1}\psi_1^{} & \hdots &
        \varphi_{n-1}\psi_{n-1}^{} & \\
    \end{bmatrix}.
\end{split}
\end{equation}

%###############################################################################


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Transformação Linear}
\label{al:tl}
Uma transformação linear é uma função entre dois espaços vetoriais. Dado $T$
uma transformação linear que mapeia o espaço vetorial $\mathbb{C}^{n}$ para o
espaço vetorial $\mathbb{C}^{m}$ podemos escrever
$T:\mathbb{C}^{n}\rightarrow\mathbb{C}^{m}$, e temos as seguintes propriedades:

\begin{description}
    \item[Preservação da soma]
    \begin{equation}
        T(\ket{\varphi} + \ket{\psi}) = T\ket{\varphi} + T\ket{\psi};
    \end{equation}
    \item[Preservação da multiplicação por escalar]
    \begin{equation}
        T(\alpha\ket{\varphi}) = \alpha T\ket{\varphi}.
    \end{equation}
\end{description}
Além disso, quando o domínio e o contradomínio da transformação linear são
iguais, ela também é chamada de operador linear.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\subsubsection*{Funcional linear}
Um funcional linear é um tipo específico de transformação linear que mapeia um
espaço vetorial à escalares. Por exemplo, dado $\ket{\varphi}$ e $\ket{\psi}
\in \mathbb{C}^{n}$, um bra $\bra{\varphi}$ pode ser definido como um funcional
linear tal que $\bra{\varphi}:\mathbb{C}^{n}\rightarrow\mathbb{C}$, desta
forma, quando operado com $\ket{\psi}$ resulta em um escalar de valor complexo
$\braket{\varphi}{\psi}$.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\subsubsection*{Projeção}
\label{al:projecao}
A projeção se utiliza do produto interno, então de maneira semelhante temos uma
definição para quando se tratando de vetores reais e outra para vetores
complexos.
A projeção de $\ket{\varphi}$ em $\ket{\psi}$ (para $\ket{\varphi}, \ket{\psi}$
em $\mathbb{R}^2$ ou $\mathbb{R}^2$) retorna um vetor paralelo a $\ket{\psi}$
com tamanho proporcional ao tamanho de $\ket{\varphi}$ e o cosseno do ângulo
entre os vetores.

Em $\mathbb{C}^n$, temos que, $\braket{\psi}{\varphi}$ é o coeficiente de
projeção de $\ket{\varphi}$ na direção $\ket{\psi}$. Então temos que
$\braket{\psi}{\varphi}\ket{\psi}$ é a projeção do vetor $\ket{\varphi}$ na
direção $\ket{\psi}$, ou seja:
\begin{equation}
    \text{proj}_{\ket{\psi}}\ket{\varphi} = \braket{\psi}{\varphi}\ket{\psi}
    = \ketbra{\psi}{\psi}\ket{\varphi},
\end{equation}
aqui $\ketbra{\psi}{\psi}$ é um operador de projeção na direção
$\ket{\psi}$.

A partir disso, derivamos a relação de completude que nos permite facilmente
escrever qualquer vetor $\ket{\varphi}$ como a soma das suas projeções
ortogonais com relação a uma base ortonormal. Por exemplo, dado $\beta = \{
    \ket{b_0}, \ket{b_1}, \hdots, \ket{b_{n-1}}\}$ uma base ortonormal, podemos
escrever $\ket{\varphi}$ como:

\begin{equation}
\begin{split}
    \ket{\varphi} = \sum_{i = 0}^{n-1} \text{proj}_{\ket{b_i}}\ket{\varphi} =
    \sum_{i = 0}^{n-1} \braket{b_i}{\varphi}\ket{b_i} \\
    = \sum_{i = 0}^{n-1} \ketbra{b_i}{b_i}\ket{\varphi} =
    I\ket{\varphi} = \ket{\varphi}.
\end{split}
\end{equation}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\subsection{Operadores Relevantes}
Nesta seção, é apresentado um conjunto de operadores relevantes aos estudos da
computação quântica.

\subsubsection*{Operadores Adjuntos}
\label{al:operadores}

Dado um espaço de Hilbert $V$, o operador adjunto $A^\dagger$ (também chamado de
conjugado Hermitiano) do operador $A$ é tal que para todos os vetores
$\ket{\varphi}, \ket{\psi} \in V$ vale:
\begin{equation}
\begin{split}
    (\ket{\varphi}, A\ket{\psi}) = \bra{\varphi}A\ket{\psi} =
    (\bra{\varphi}A)\ket{\psi} = (A^\dagger\ket{\varphi},\ket{\psi}).
\end{split}
\end{equation}

Seguindo a definição temos que $(AB)^\dagger = B^\dagger A^\dagger$, e
$\ket{\varphi}^\dagger = \bra{\varphi}$. Desta forma, temos que:
$(A\ket{\varphi})^\dagger = \ket{\varphi}^\dagger A^\dagger =
\bra{\varphi}A^\dagger$. Ao considerar a representação matricial do operador
$A$, temos que sua relação com $A^\dagger$ é dada por: $ A^\dagger = (A^*)^T$,
o que possibilita observar que $(A^\dagger)^\dagger = A$.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\subsubsection*{Operadores Normais}
\label{al:op:norma}
Um operador $A$ é dito normal se:
\begin{equation}
    A^\dagger A = AA^\dagger,
\end{equation}
além disso, pelo teorema da decomposição espectral temos que um operador é
normal se e somente se é diagonalizável.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\subsubsection*{Operadores Autoadjuntos}
Um operador $A$ que tem como operador adjunto o próprio $A$ é chamado de
Autoadjunto (ou Hermitiano):
\begin{equation}
  A^\dagger = A.
\end{equation}
Por ter tal propriedade temos que um operador Hermitiano também é normal:
\begin{equation}
    AA^\dagger = A^\dagger A = A^2,
\end{equation}
além disso, seguindo o teorema da decomposição espectral de operadores
hermitianos, temos que: um operador é Hermitiano se e somente se seus
autovalores são reais.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\subsubsection*{Operadores Unitários}
Um operador $A$ é unitário se segue que:
\begin{equation}
    AA^\dagger = A^\dagger A = I.
\end{equation}
Dado que $AA^\dagger = A^\dagger A$, operadores unitários são automaticamente
operadores normais. E pelo teorema da decomposição espectral de operadores
unitários temos que dado um operador normal, ele é unitário se e somente se
seus autovalores são números complexos módulo 1.
Os operadores unitários se mostram úteis por preservar o produto interno entre
vetores:
\begin{equation}
    (A\ket{\varphi}, A\ket{\psi}) = \bra{\varphi}A^\dagger A\ket{\psi} =
    \bra{\varphi}I\ket{\psi} = \bra{\varphi}\ket{\psi} =
    (\ket{\varphi}, \ket{\psi}).
\end{equation}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\subsubsection*{Operadores Positivos}
Os operadores positivos são uma subclasse dos operadores Hermitianos, tal que,
um operador $A$ é positivo se para todo $\ket{\varphi}$:
\begin{equation}
    \bra{\varphi}A\ket{\varphi} \geq 0,
\end{equation}
pelo teorema espectral temos que para todo operador positivo seus autovalores são
reais (já que ele também é Hermitiano) e não negativos.
Um operador positivo pode também ser um operador positivo definido:
\begin{equation}
    \bra{\varphi}A\ket{\varphi} > 0,
\end{equation}
para todo $\ket{\varphi}$ não nulo. Nesse caso, pelo teorema espectral temos que
em todo operador positivo definido, seus autovalores são reais e positivos.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\subsubsection*{Operadores de Projeção}
Um operador de projeção $A$ leva o espaço vetorial $V$ em algum subespaço $W$
contido em $V$. Além disso, um operador de projeção $A$ satisfaz:
\begin{equation}
    A^2 = A.
\end{equation}
Dado o subespaço vetorial $k$-dimensional $W$ do espaço $l$-dimensional $V$ ($l
\geq k$) , é possível obter $\{\ket{\varphi_0}, \hdots, \ket{\varphi_k},
\hdots, \ket{\varphi_l}\}$, base ortonormal de $V$, de forma que
$\{\ket{\varphi_0}, \hdots, \ket{\varphi_k}\}$ seja base ortonormal de $W$ e o
operador $A$ de projeção para $W$ seja definido por:
\begin{equation}
    \text{proj}_W = A = \sum_{i = 0}^k \ketbra{\varphi_i}{\varphi_i},
\end{equation}
pela definição podemos ver que $\ketbra{\varphi_i}{\varphi_i}$ é Hermitiano:
\begin{equation}
    (\ketbra{\varphi_i}{\varphi_i})^\dagger  =
    \bra{\varphi_i}^\dagger\ket{\varphi_i}^\dagger =
    \ketbra{\varphi_i}{\varphi_i},
\end{equation}
portanto, $A$ é Hermitiano e $A^\dagger = A$. Também temos que, para cumprir
$A^2 = A$, a matriz diagonal de $A$ também precisa ser um operador de projeção e
portanto, os autovalores de $A$ são 0 ou 1. E desta forma, todo operador de
projeção é também, um operador positivo.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\subsection{Produto Tensorial}
\label{al:pt}
O produto tensorial é uma operação que compõe um espaço vetorial a partir de
outros dois espaços vetoriais. Essa operação é importante por permitir compor
sistemas quânticos com mais de um qubit (\cref{subsec:sistemacomposto}).

Dado um espaço vetorial $V$ de dimensão $m$ e um espaço vetorial $W$ de
dimensão $n$, o espaço vetorial $V$ tensorial $W$ (denotado por $V\otimes W$)
possui dimensão $nm$. E um elemento de $V\otimes W$ é adquirido através do
produto tensorial $\ket{v}\otimes\ket{w}$, onde, $\ket{v}\in V$ e $\ket{w}\in
W$. Além disso podemos usar as seguintes notações para $\ket{v}\otimes\ket{w}$:
$\ket{v}\ket{w}, \ket{v, w} e \ket{vw}$.

Temos também que, dados $\ket{v},\ket{v'} \in V$, $\ket{w},\ket{w'}\in W$ e o
escalar $\alpha$ o produto tensorial possui as seguintes propriedades:
\begin{description}
    \item
    \begin{equation}
        \alpha (\ket{v}\otimes\ket{w}) = (\alpha\ket{v})\otimes\ket{w} =
        \ket{v}\otimes (\alpha\ket{w});
    \end{equation}
    \begin{equation}
        (\ket{v}+\ket{v'}) \otimes \ket{w} =
        \ket{v}\otimes\ket{w} + \ket{v'}\otimes\ket{w};
    \end{equation}
    \begin{equation}
        \ket{v}\otimes (\ket{w}+\ket{w'}) =
        \ket{v}\otimes\ket{w} + \ket{v}\otimes\ket{w'}.
    \end{equation}
\end{description}

Agora que temos um novo espaço vetorial, precisamos de novos operadores
lineares para operarmos sobre $V\otimes W$. Dado um operador $A$ sobre $V$ e um
operador $B$ sobre $W$, podemos definir o operador $A\otimes B$ sobre $V\otimes
W$:
\begin{equation}
    (A\otimes B) (\ket{v}\otimes\ket{w}\equiv A\ket{v}\otimes B\ket{w},
\end{equation}
podemos visualizar melhor o operador $A\otimes B$ na representação do produto
de \textit{Kronecker}. Dada as matrizes $A_{m\times n}$ e $B_{p\times q}$ ,
$A\otimes B$ é uma matriz $mp\times nq$:
\begin{equation}
    A\otimes B \equiv
    \left.\left[
    \vphantom{\begin{array}{c}1\\1\\1\\1\\1\end{array}}
    \smash{\underbrace{
        \begin{array}{cccc}
            A_{11}B & A_{12} & \hdots & A_{1n}B \\
            A_{21}B & A_{22} & \hdots & A_{2n}B \\
            \vdots  & \vdots & \ddots & \vdots  \\
            A_{m1}B & A_{m2} & \hdots & A_{mn}B
        \end{array}
    }_{n\times q}}
    \right]\right\}\,m\times p
\end{equation}
\vspace{0.3cm}

Outra notação usada é $\ket{\psi}^{\otimes n}$, quando fazemos produto
tensorial sobre o elemento $\ket{\psi}$ $n$ vezes:
\begin{equation}
    \begin{split}
        \ket{\psi}^{\otimes n} = \ket{\psi}^{\otimes n-1}\otimes\ket{\psi}, \\
        \ket{\psi}^{\otimes 2} = \ket{\psi}\otimes\ket{\psi}.
    \end{split}
\end{equation}

Dada a revisão dos conceitos de álgebra linear, agora serão apresentados os
conceitos da mecânica quântica.
