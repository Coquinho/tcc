def make_line(line):
    new_line = ""
    for word in line:
        new_line += word + " "
    return (new_line[:-1] + "\n")

output = ""
with open("results0beyond.txt","r") as f:
    line = f.readline()
    while(line):
        line = list(line.split())
        if line[1] == "0":
            output += make_line(line)
        elif line[1] == "1":
            new_line = []
            new_line.append(line[0])
            new_line.extend(line[3:5])
            new_line.extend(line[1:3])
            output += make_line(new_line)
        # pick next line
        line = f.readline()

with open("results0beyond.txt","w") as f:
    f.write(output)
