# Entrada dos valores obtidos
format long E
real = load("real0beyondtovec.txt");

# Geração dos tamanhos das entradas
vals = 0:0.0001:306;

# Geração dos valores esperados
theta = 2*pi/11;
aux = cos(vals*theta);
esp = aux.*aux;
esp = esp.*100;

espToError = esp(1:10000:end)

erro = abs(espToError-real');

# Erro maximo
[maxerro, i] = max(erro);
maxerro = maxerro
value = vals(i)

# Erro Min
[minerro, i] = min(erro);
minerro = minerro
value = vals(i)

# Estatisticas
media = sum(erro)/size(erro)(2)
variancia = sum((erro-media).*(erro-media))/size(erro)(2)
desvio = sqrt(variancia)
size(erro)

# Grafico
plot(0:306,real, 'b', vals,esp, 'r', 0:306, erro,'k')
#plot(0:306, erro,'k')
title ("Valores obtidos VS valores de aproximação");
txt = legend("Valores\naproximados", "Valores\nesperados", "Erro");
legend boxoff
legend (txt, "location", "northeastoutside");
xlabel ("Tamanho da palavra");
ylabel ("Probabilidade de ser aceita");
set (txt, "fontsize", 20);


