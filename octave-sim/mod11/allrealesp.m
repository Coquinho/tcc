format long E
real = load("real.txt");
real = 100-real;
real = real';

vreal = 0:11:1000;
vreal = [vreal 3:11:1000];
vreal = sort(vreal);

size(vreal)
size(real)

clf;

#plot(vreal,real, 'xb', vesp,esp, 'r')

esp = [];
for i = vreal
  theta = i*2*pi/11;
  aux = cos(theta);
  esp = [esp aux*aux*100];
endfor
esp;

plot(vreal,real, 'xb', vreal,esp*.85+8.5, 'or')
#plot(vreal,real, 'xb', vreal,esp, 'or')

title ("Valores obtidos VS valores esperados");
txt = legend("Valores\nreais", "Valores\nesperados");
legend boxoff
legend (txt, "location", "northeastoutside");
xlabel ("Tamanho da palavra");
ylabel ("Probabilidade de ser aceita");
set(txt, "fontsize", 30);
set(gca,  "fontsize", 30)
