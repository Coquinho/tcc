\subsubsection*{Qubit - O bit quântico}
\label{sec:qubit}
O qubit é o exemplo mais simples de um sistema quântico fechado, mas antes de
entender o que é um qubit, é interessante entender sua versão clássica, o bit.
Em um sistema computacional clássico o bit é o menor pedaço de informação
existente. Ele possui dois estados possíveis ou interpretações, como por
exemplo: ligado ou desligado, ``0"\ ou ``1"\ e verdadeiro ou falso. Combinando
mais bits podemos ter mais estados e, portanto, mais interpretações. Dessa
forma, podemos guardar mais informação e processa-la de mais formas. A
quantidade de informações diferentes armazenada em um conjunto de bits cresce
de maneira exponencial, ou seja, ao adicionar um bit é possível representar o
dobro de informação.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\subsubsection*{O bit quântico}
\label{qubit:qubit}

O bit quântico, qubit, \cite{nielsen2010quantum} é um conceito análogo ao bit
clássico. Ele representa o menor pedaço de informação que um sistema quântico
pode representar. Os qubits, neste trabalho, serão objetos matemáticos, mas
assim como os bits podem ser interpretados em sistemas físicos de formas
variadas e implementados de diferentes formas.

Um qubit, assim como um bit, possui 2 estados como por exemplo $\ket{0}$ e
$\ket{1}$, análogos ao ``0"\ e ``1"\ clássicos. Porém, existem mais estados
possíveis para um qubit. Um qubit $\ket{\psi}$ arbitrário pode estar em uma
combinação linear dos estados, uma superposição; desta forma, $\ket{\psi}$ é
descrito por:
\begin{equation}
\label{qubit:eq:state}
\ket{\psi} = \alpha \ket{0} + \beta \ket{1},
\end{equation}
onde $\alpha$ e $\beta$ são valores que representam a amplitude das projeções de
$\ket{\psi}$ nos estados $\ket{0}$ e $\ket{1}$, respectivamente. De forma
simplificada, representam a proporção de $\ket{\psi}$ nos estado $\ket{0}$ e
$\ket{1}$. Em geral $\alpha$ e $\beta$ serão números complexos. $\ket{0}$ e
$\ket{1}$ são vetores colunas de uma base ortonormal que representam os estados
análogos aos estados ``0"\ e o ``1"\ do bit.

Por se tratar de um sistema quântico, a mecânica quântica nos garante não
termos acesso ao estado atual de um qubit, isto é, aos valores $\alpha$ e
$\beta$, tão facilmente quanto ao estado de um bit, que pode ser acessado a
qualquer momento, quantas vezes quisermos e sem perda de informação. Ao ler um
bit, as únicas respostas possíveis são ``0"\ ou ``1". De forma similar, no caso
de um qubit temos que até antes de um qubit ser medido (o equivalente a ler um
bit) ele pode estar em uma superposição dos seus estados, porém, ao ser medido
as únicas respostas possíveis são os estados base, nesse caso, $\ket{0}$ e
$\ket{1}$. Portanto, após a medida o estado colapsa para uma dessas
possibilidades e qualquer leitura feita nele, enquanto ele não for operado
novamente, sempre retornará o mesmo resultado. A mecânica quântica nos garante
que no momento da medida teremos como resultado da leitura de $\ket{\psi}$,
$\ket{0}$ com probabilidade $|\alpha|^2$ ou $\ket{1}$ com probabilidade
$|\beta|^2$, onde $|\alpha|^2$ e $|\beta|^2$ somam 1, como manda a
probabilidade. Por causa disto, lidamos sempre com nossos estados com tamanho
'1', ou seja, um vetor normalizado.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\subsubsection*{Representações de um Qubit}
\label{qubit:representacoes}

Por termos $|\alpha|^2 + |\beta|^2 = 1$, é possível reescrever o estado
$\ket{\psi}$ da \cref{qubit:eq:state} da seguinte maneira:
\begin{equation}
\label{qubit:eq:estateblochglob}
    \ket{\psi} = e^{i\gamma}(\text{cos}\frac{\theta}{2}\ket{0} +
    e^{i\varphi}\text{sin}\frac{\theta}{2}\ket{1}),
\end{equation}
onde $\gamma$, $\theta$ e $\varphi$ são números reais. É possível provar que o
fator $e^{i\gamma}$, mesmo correspondendo a uma alteração da fase global, não
possui um efeito observável, portanto a \cref{qubit:eq:estateblochglob} pode
ser reescrita como:
\begin{equation}
\label{qubit:eq:estatebloch}
    \ket{\psi} = \text{cos}\frac{\theta}{2}\ket{0} +
    e^{i\varphi}\text{sin}\frac{\theta}{2}\ket{1},
\end{equation}
onde $\theta$ e $\varphi$ são números que especificam o ângulo polar e
azimutal, respectivamente,  de forma a, juntos, definirem um ponto na
superfície de uma esfera 3-dimensional de raio 1. Essa esfera representa todas
as possibilidades de estados que um qubit pode ter, e é chamada de esfera de
Bloch (\cref{qubit:bloch}).

\begin{figure}[h]
\centering
\begin{tikzpicture}[line cap=round, line join=round, >=Triangle]
  %\clip(-4.01,-4) rectangle (4,4.01);
  \draw [shift={(0,0)}, lightgray, fill, fill opacity=0.1] (0,0) -- (56.7:0.4) arc (56.7:90.:0.4) -- cycle;
  \draw [shift={(0,0)}, lightgray, fill, fill opacity=0.1] (0,0) -- (-135.7:0.4) arc (-135.7:-33.2:0.4) -- cycle;
  \draw(0,0) circle (3cm);
  \draw [rotate around={0.:(0.,0.)},dash pattern=on 3pt off 3pt] (0,0) ellipse (3cm and 0.9cm);
  \draw (0,0)-- (0.70,1.07);
  \draw [->] (0,0) -- (0,3);
  \draw[lightgray, dash pattern=on 3pt off 3pt,->] (0,0) -- (0,-3);
  \draw [->] (0,0) -- (-1,-0.87);
  \draw[lightgray, dash pattern=on 3pt off 3pt,->] (0,0) -- (1,0.87);
  \draw (1,0.87) node[anchor=south west] {$\frac{\ket{0}-\ket{1}}{\sqrt{2}}$};
  \draw [->] (0,0) -- (3,0);
  \draw[lightgray, dash pattern=on 3pt off 3pt,->] (0,0) -- (-3,0);
  \draw [dotted] (0.7,1)-- (0.7,-0.46);
  \draw [dotted] (0,0)-- (0.7,-0.46);
  \draw (-0.08,-0.3) node[anchor=north west] {$\varphi$};
  \draw (0.01,0.9) node[anchor=north west] {$\theta$};
  \draw (-1.,-0.87) node[below] {$\frac{\ket{0}+\ket{1}}{\sqrt{2}}$};
  \draw (3,0) node[anchor= west] {$\frac{\ket{0}+i\ket{1}}{\sqrt{2}}$};
  \draw (-3,0) node[anchor= east] {$\frac{\ket{0}-i\ket{1}}{\sqrt{2}}$};
  \draw (0,3) node[above] {$\ket{0}$};
  \draw (0,-3) node[below] {$\ket{1}$};
  \draw (0.7,1) node[above] {$\ket{\psi}$};
  \scriptsize
  \draw [fill] (0,0) circle (1.5pt);
  \draw [fill] (0.7,1.1) circle (0.5pt);
\end{tikzpicture}
    \caption{Representação de um qubit na esfera de Bloch.}
    \label{qubit:bloch}
\end{figure}

Na esfera temos que $\ket{0}$ se encontra ao norte do eixo $z$ e $\ket{1}$ ao
sul; variando o ângulo azimutal variamos tanto no eixo $x$, entre
$\frac{\ket{0}+\ket{1}}{\sqrt{2}}$ e $\frac{\ket{0}-\ket{1}}{\sqrt{2}}$, quanto
no eixo $y$, entre $\frac{\ket{0}+i\ket{1}}{\sqrt{2}}$ e
$\frac{\ket{0}-i\ket{1}}{\sqrt{2}}$.
A esfera de Bloch é uma abstração útil para visualização de 1 qubit por
permitir interpretar as variações do estado de um qubit, como rotações
aplicadas com relação aos ângulos $\theta$ e $\varphi$.

Um qubit pode estar em qualquer ponto da esfera de Bloch, portanto possui
infinitas possibilidades de estados, o que pode facilmente nos fazer pensar que
um qubit pode armazenar informação infinita. Porém, como ao medir um qubit só
temos duas possíveis respostas, não temos realmente informação infinita
armazenadas em um qubit. Por outro lado, antes da medida, realmente podemos ter
mais informação do que $\ket{0}$ e $\ket{1}$ como veremos posteriormente.

