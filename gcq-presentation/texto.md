------------------------ Intro --------------------------

Olá, Sou Lucas Calcante de Sousa e orientado pelo Prof. Eduardo Inácio Duzzioni
pesquisei sobre Modelos Computacionais Quânticos.
Um modelo computacional é uma forma de computar, ou seja, uma forma de
solucionar um problema.
Na Ciência da Computação o estudo dos modelos computacionais trouxe um
entendimento sobre como se soluciona problemas com um computador e que tipo de
problemas são solucionáveis com eles.
Neste estudo o objetivo foi obter um maior entendimento sobre esses mesmos
pontos, porém observando versões quânticas dos modelos computacionais clássicos.
Quando digo clássico, me refiro aos modelos computacionais baseados na física
clássica, como, os computadores e smartphones. E quando digo quântico, me
refiro a contrapartida baseadas nos efeitos físicos da mecânica quântica.
Ou seja, o objetivo desse trabalho foi obter um maior entendimento sobre como
se resolve problemas utilizando os efeitos da mecânica quântica.
O modelo estudado foi o MO1QFA, que é uma das versões quânticas dos Autômatos
Finitos, um dos modelos computacionais mais simple.

----------------------- AF ------------------------------

Os Autômatos Finitos reconhecem uma linguagem, ou seja, dada uma palavra de
entrada, a saída dessa computação é dizer se a palavra pertence ou não a
linguagem do autômato.

Por exemplo, aqui temos um autômato que reconhece palavras constituídas de a's
e b's que possuem uma quantidade par de a's, sem a's consecutivos, ou seja,
entre dois a's sempre existe ao menos um b.

Como esse autômato funciona? As bolas representam estados, indicam em que ponto
está a computação; as flechas são as transições, elas indicam uma mudança de
estado dado um gatilho. Por exemplo, em q0 se o autômato ler um b ele transita
para o próprio q0, se ele ler um a ele transita para q1.

Os estados que possuem círculos circunscritos, como q3, são estados finais,
caso a computação termine em um estado final, podemos dizer que o autômato
aceitou a palavra computada, ou seja, a palavra pertence a linguagem, caso a
computação não termine em um estado final dizemos que o autômato rejeitou a
palavra, ou seja, a palavra não pertence a linguagem.

Além disso, a seta sem origem indica em que estado a computação começa, nesse
caso q0.

Para computar aba ele passa pelo seguinte processo, inicia em q0, lê o primeiro
a transita para q1, lê um b e, portanto, transita para q2, e lê o último a e
transita para q3. Como já leu toda a palavra, ele analisa o estado, como o
último estado foi q3, que é final, a palavra é aceita.

Agora um exemplo de erro, a palavra bbaa. O autômato inicia em q0, lê um b e
continua em q0, lê o segundo b e continua em q0, lê o primeiro a e transita
para q1, de q1 com a não existe uma transição definida e portanto o automato
retorna que rejeita a palavra.

----------------------- MO1QFA ------------------------------

O MO1QFA introduz efeitos quânticos ao autômato finito. Mais especificamente as
evoluções e as medidas.

A evolução de um estado quântico é descrita por uma
matriz unitária, por simplificação, aqui no diagrama as probabilidades
descritas nas matrizes unitárias estão descritas nas transições.  Dessa forma,
dado o estado q0 e um a na entrada, existe cosseno de menos raiz de dois de
chance dele transitar para q0 e seno de raiz de menos 2 de chance dele
transitar para q1. Permitindo que o estado esteja em uma superposição.

O outro efeito quântico é a medida, adicionada justamente para lidar com as
superposições. Durante a computação o estado atual pode ser uma superposição de
q0 e q1, porém ao fim da computação ocorre a medida e, após ela, o estado
colapsa para uma das possibilidade q0 ou q1.

Como o autômato possui dois estados, podemos utilizar somente um qubit para
representar o estado atual, e utilizar matrizes 2 por 2 para indicar, por meio
de rotações, as transições do automato. Dessa forma, a computação
desse autômato é dada por rotações em um qubit.

Um qubit pode ser representado pela esfera de bloch, nela podemos ver todos os
estados possíveis de um qubit.
