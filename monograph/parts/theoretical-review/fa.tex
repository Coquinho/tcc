\subsection{Autômatos Finitos}
\label{mcc:dfa}
Em geral, um autômato finito (AF) é representado por um diagrama de estados,
por exemplo, um AF que aceita a linguagem $L$, que é o conjunto das palavras
constituídas somente por símbolos $a$ e $b$ onde a quantidade de $a$'s é par e
não existem 2 $a$'s consecutivos pode ser representado como na
\cref{fig:af:diagrama:qtdapar}.
\begin{figure}[h] % ’ht’ tells LaTeX to place the figure ’here’ or at the top of the page
    \centering % centers the figure
    \begin{tikzpicture}[->, >=stealth, node distance=2.5cm, every state/.style={thick, fill=gray!10},initial text=$ $]
       \node[state, initial, accepting] (0) {$q_0$};
        \node[state, right of=0] (1) {$q_1$};
        \node[state, below of=1] (2) {$q_2$};
        \node[state, left of=2, accepting] (3) {$q_3$};
        \draw
            (0) edge[loop above] node{b} (0)
            (0) edge[bend left, above] node{a} (1)
            (1) edge[bend left, right] node{b} (2)
            (2) edge[loop right, right] node{b} (2)
            (2) edge[bend left, below] node{a} (3)
            (3) edge[bend left, left] node{b} (0);
    \end{tikzpicture}

    \caption{Autômato $M_0$ que reconhece uma quantidade par de $a$'s, onde não existem 2 $a$'s consecutivos.}
    \label{fig:af:diagrama:qtdapar}
\end{figure}

A estrutura básica de um autômato é constituída de estados e de transições. Os
estados são as circunferências ($q_0$, $q_1$, $q_2$, $q_3$), que indicam
pontos da computação. Além disso, os estados podem ser ainda mais específicos,
sendo um estado inicial ou final. O estado inicial, representado pela seta
($q_0$), é onde se inicia a computação do autômato, o estado que o autômato se
encontra no tempo 0. Os estados finais, representado pelas circunferências
concêntricas ($q_0$, $q_3$), são estados que indicam se a entrada será aceita
ou não ao final da computação. Além dos estados, um autômato também possui
transições, representadas por setas ligando estados. As transições mostram que
a partir de um determinado estado, com a leitura de um determinado símbolo o
estado será alterado para o apontado pela seta.

Durante seu funcionamento, um autômato inicia no estado inicial, e lê a palavra
de entrada, símbolo a símbolo da esquerda para direita, e a cada símbolo segue
as transições do estado atual para alterar de estado. Se ao fim da palavra, o
estado for final, a entrada é dita aceita e, portanto reconhecida pelo
autômato. Caso ele não seja final, a palavra é dita rejeitada e, portanto não
reconhecida pelo autômato.

Por exemplo, ao receber $aba$ como entrada, $M_0$ passa pelo seguinte
processamento:
\begin{enumerate}
   \item Inicia no estado $q_0$;
   \item Lê $a$ e transita de $q_0$ para $q_1$;
   \item Lê $b$ e transita de $q_1$ para $q_2$;
   \item Lê $a$ e transita de $q_2$ para $q_3$;
   \item Aceita, já que ao final do processamento da entrada $M_0$ se encontra
      no estado $q_3$, que é final.
\end{enumerate}

E ao receber a palavra $ababba$ como entrada, $M_0$ passa pelo seguinte
processamento:
\begin{enumerate}
   \item Inicia no estado $q_0$;
   \item Lê $a$ e transita de $q_0$ para $q_1$;
   \item Lê $b$ e transita de $q_1$ para $q_2$;
   \item Lê $a$ e transita de $q_2$ para $q_3$;
   \item Lê $b$ e transita de $q_3$ para $q_0$;
   \item Lê $b$ e transita de $q_0$ para $q_0$;
   \item Lê $a$ e transita de $q_0$ para $q_1$;
   \item Rejeita, já que ao final do processamento da entrada $M_0$ se encontra
      no estado $q_1$, que não é final.
\end{enumerate}

\subsubsection*{Definição Formal}
\begin{ddef}
   Um autômato finito é dado pela quíntupla $M = (Q,\Sigma,\delta,q_0,F)$:
   \begin{description}
        \item[$Q$] é o conjunto finito dos estados;
        \item[$\Sigma$] é o conjunto finito do alfabeto de entrada;
        \item[$\delta$]$:Q\times\Sigma\rightarrow Q$ é a função transição;
        \item[$q_0$] $\in Q$ é o estado inicial do autômato;
        \item[$F$] $\subseteq Q$ é o conjunto de estados finais.
   \end{description}
\end{ddef}

A função de transição $\delta$ define um mapeamento de cada estado,
sobre cada símbolo, para um estado. Para evitar a poluição dos autômatos, em
geral se omitem as transições irrelevantes. Por exemplo, na
\cref{fig:af:diagrama:qtdapar}, podemos ver que para $q_1$ não existe nenhuma
transição definida pelo símbolo $a$, isto é, se o estado atual é
$q_1$, ao ler um $a$ da entrada se deve rejeitar a entrada.
Em alguns momentos pode ser interessante a representação  completo, ou seja, o
autômato com todas as transições.

\begin{figure}[h] % ’ht’ tells LaTeX to place the figure ’here’ or at the top of the page
    \centering % centers the figure
    \begin{tikzpicture}
       [->, >=stealth, node distance=2.5cm, every state/.style={thick, fill=gray!10},initial text=$ $]
        \node[state, initial, accepting] (0) {$q_0$};
        \node[state, right of=0] (1) {$q_1$};
        \node[state, below of=1] (2) {$q_2$};
        \node[state, left of=2, accepting] (3) {$q_3$};
        \node[state] (err) at ($(0)!0.5!(2)$) {$err$};
        \draw
            (0) edge[loop above] node{b} (0)
            (0) edge[bend left, above] node{a} (1)
            (1) edge[bend left, right] node{b} (2)
            (1) edge[bend left, right] node{a} (err)
            (2) edge[loop right, right] node{b} (2)
            (2) edge[bend left, below] node{a} (3)
            (3) edge[bend left, left] node{b} (0)
            (3) edge[bend right, left] node{a} (err)
            (err) edge[loop above] node{a,b} (err);
    \end{tikzpicture}

    \caption{$M_1$, versão completa de $M_0$.}
    \label{fig:af:definicaoformal:qtdapar}
\end{figure}
Na \cref{fig:af:definicaoformal:qtdapar} podemos ver que $M_1$ é um autômato
completo: todos os estados possuem transições definidas com todos os símbolos de
entrada, e que para continuar com o mesmo propósito, foi criado um estado
$err$ de erro, para o qual todas as transições que representam uma má
formação com relação a regra de formação das palavras aceitas vão.

\subsubsection*{Outras formas de representação}
Existem outras formas de se representar um autômato, este trabalho apresenta a
representação por meio de tabela e usará a por meio da notação de
\textit{Dirac}, que é uma representação mais próxima das usadas nos autômatos
finitos quânticos.

Na \cref{tab:af:definicaoformal:qtdapar}, podemos ver um AF com sua função de
transição em forma de tabela. Partindo de qualquer estado (primeira coluna),
para qual estado $M_0$ transita ao receber cada símbolo de entrada (segunda e
terceira coluna). Podemos ver também o estado inicial, que possui uma
$\rightarrow$ ao seu lado e, os estados finais, que possuem um $*$.

\begin{table}[h]
\centering
\begin{tabular}{|c|c|c|}
\hline
$\delta$        & a       & b      \\ \hline
$\rightarrow q_0$ & $q_1$ & $q_0$  \\
$q_1$           & -       & $q_2$  \\
$q_2$           & $q_3$   & $q_2$  \\
$q_3$           & -       & $q_0$  \\ \hline
\end{tabular}
    \caption{Função transição de $M_0$.}
    \label{tab:af:definicaoformal:qtdapar}
\end{table}

Neste trabalho utilizaremos a notação de \textit{Dirac} como forma de
representação dos autômatos finitos. Utilizando a notação de \textit{Dirac}
podemos ignorar os efeitos da mecânica quântica e aproximar os AF's visto até
agora para suas versões quânticas. Nessa representação os estados serão
vetores:
\begin{equation}
    q_0 = \ket{0};
    q_1 = \ket{1};
    q_2 = \ket{2};
    q_3 = \ket{3},
\end{equation}
a função transição será dada através de um conjunto de matrizes, uma para cada
símbolo de entrada:
\begin{equation}
\label{eq:af:matrix:qtdapar}
\begin{split}
   A =
      \begin{bmatrix}
         0 & 0 & 0 & 0 \\
         1 & 0 & 0 & 0 \\
         0 & 0 & 0 & 0 \\
         0 & 0 & 1 & 0 \\
      \end{bmatrix};
   B =
      \begin{bmatrix}
         1 & 0 & 0 & 1 \\
         0 & 0 & 0 & 0 \\
         0 & 1 & 1 & 0 \\
         0 & 0 & 0 & 0 \\
      \end{bmatrix},\\
\end{split}
\end{equation}
onde a matriz $A$(resp. $B$) da \cref{eq:af:matrix:qtdapar} representa todas as
transições que possuem o símbolo $a$ (resp. $b$) como gatilho. A posição
A(i,j) (resp. B(i,j)), $i$-ésima linha e $j$-ésima coluna, de A (resp. B) tem o
valor 1 quando o estado $j$ transita para o estado $i$ por $a$ (resp. $b$), e
possui o valor 0 caso contrário.

A transição de estados é dada pela multiplicação do estado atual pela matriz de
transição do símbolo de entrada sendo lido, desta forma, dada que o autômato se encontra no estado $\ket{3}$ e lê um $b$ ele transita para o seguinte estado:
\begin{equation}
  B\ket{3} = \ket{0} = q_0.
\end{equation}
Dada uma computação intermediária, de $ababba$ partindo do estado $\ket{2}$, o
estado final da computação será:
\begin{equation}
    ABBABA\ket{2} = \ket{3} = q_3,
\end{equation}
de forma similar, podemos definir operadores de projeção (\cref{al:projecao})
dos estados finais para avaliar se a palavra é aceita:
\begin{equation}
    P_\text{acc} = \sum_{i\in F} \ketbra{i}{i}.
\end{equation}
Agora podemos definir a computação como a multiplicação do operador de projeção
dos estados finais pela computação apresentada anteriormente, caso o resultado
seja 1, a palavra é aceita, caso o resultado a palavra seja 0, ela é rejeitada.
Desta forma a computação de $ababba$ é dada por:
\begin{equation}
    P_\text{acc}ABBABA\ket{0} = 0,
    \label{sec:fa:exemplo:comp}
\end{equation}
e portanto, $ababba$ não é aceita por $M_0$.
Podemos observar na \cref{sec:fa:exemplo:comp} que as matrizes estão na ordem
inversa da palavra, isso se deve pela ordem da aplicação das matrizes, uma
matriz afeta o estado a sua direita. Portanto, quanto mais próximo do início da
palavra está uma letra mais a direita na equação da computação estará sua
matriz de transição.

O autômato finito visto até o momento é chamado de autômato finito
determinístico, pois para cada par de estado e símbolo de entrada existe uma
transição, com a notação de \textit{Dirac} isso se concretiza possibilitando
somente um valor 1 por coluna nas matrizes de transição. Existem outras versões
do autômato finito como o autômato finito não
determinístico\cite{sipse2012introduction} e o autômato finito
probabilístico\cite{rabin1963probabilistic}.  Essas versões não são relevantes
para o presente trabalho, porém, são interessantes para demonstrar a
simplicidade da notação de \textit{Dirac} nos autômatos.

Os autômatos finitos não determinísticos podem definir múltiplas transições
partindo de um mesmo estado pelo mesmo símbolo de entrada, o que permite ele a
estar em múltiplos estados em um dado instante da computação. Podemos adicionar
o não determinismo nas matrizes de transição simplesmente permitindo mais de um
valor 1 em suas colunas. Ao aplicar o operador de projeção dos estados
finais, caso o resultado seja maior ou igual a 1 a palavra é aceita.

Já os autômatos probabilísticos possuem uma probabilidade atrelada a cada
transição, onde as soma desses valores atrelados às transições partindo de um
mesmo estado e, por um mesmo símbolo somam 1. Podemos descrever autômatos
probabilísticos ao permitir múltiplos valores em uma mesma coluna, onde a soma
destes, sempre somam 1. Ao aplicar o operador de projeção dos estados finais,
obtemos a probabilidade da palavra ser aceita.

\subsubsection*{Problema da paridade}
\label{mcc:dfa:ex:paridade}
O problema da paridade consiste em dizer se uma determinada parte da palavra
ocorre uma quantidade par ou ímpar de vezes. Por simplicidade, vamos considerar
a substring $a$ em um alfabeto de $a$'s e $b$'s. Assim nosso autômato $M$ pode
ser definido por $M = (Q,\Sigma,\delta,q_0,F)$:
\begin{description}
    \item[Q] é \{$\ket{0}$,$\ket{1}$\};
    \item[$\Sigma$] é \{a,b\};
    \item[$\delta$] é dado pelas seguintes matrizes de dimensões $2 \times 2$:
        \begin{equation}
        \begin{split}
            A = \ketbra{0}{1} + \ketbra{1}{0};\\
            B = I;
        \end{split}
        \end{equation}
        onde $I$ é a matriz identidade;
    \item[$q_0$] é $\ket{0}$;
    \item[F] é $\{\ket{0}\}$.
\end{description}
O autômato se encontra no estado $\ket{0}$(resp. $\ket{1}$) quando possui uma
quantidade par(resp. ímpar) de $a$'s. A matriz $A$ faz esse papel de alternar
entre os estados a cada $a$, e a matriz $B$ deixa o estado inalterado. Se ao
final da palavra houver uma quantidade par de $a$'s o estado após a computação
será $\ket{0}$, um estado final, portanto ao aplicar o operador de projeção dos
estados finais o resultado será 1 e a palavra é aceita. Caso contrário, o
resultado será 0, portanto, a palavra será rejeitada.

A computação de $ababab$ é dada por:
\begin{equation}
    P_\text{acc}BABABA q_0,
\end{equation}
onde $P_\text{acc}$ é dado por:
\begin{equation}
  P_\text{acc} = \sum_{i\in F} \ketbra{i}{i} = \ketbra{0}{0},
\end{equation}
e computação seria:
\begin{equation}
\begin{split}
    P_\text{acc}BABABA q_0 = P_\text{acc}BABABA \ket{0} \\
    = P_\text{acc}BABAB \ket{1} = P_\text{acc}BABA \ket{1}  \\
    = P_\text{acc}BAB \ket{0} = P_\text{acc}BA \ket{0} \\
    = P_\text{acc}B \ket{1} = \ketbra{0}{0} \ket{1} = 0;
\end{split}
\end{equation}
como o resultado é 0, $ababab$ não é aceita por $M$.

Vamos agora considerar $bbbabaaa$ e sua computação:
\begin{equation}
\begin{split}
    P_\text{acc}AAABABBB q_0 = P_\text{acc}AAABABBB \ket{0} \\
    = P_\text{acc}AAABABB \ket{0} = P_\text{acc}AAABAB \ket{0} \\
    = P_\text{acc}AAABA \ket{0} = P_\text{acc}AAAB \ket{1} \\
    = P_\text{acc}AAA \ket{1} = P_\text{acc}AA \ket{0} \\
    = P_\text{acc}A \ket{1} = \ketbra{0}{0} \ket{0} = 1,
\end{split}
\end{equation}
como temos o resultado 1, $bbbabaaa$ é aceita por $M$.

É interessante notar que este AF é facilmente convertido para aceitar uma
quantidade ímpar de $a$'s, bastando somente alterar o conjunto de estados
finais para $F = \{\ket{1}\}$ ao invés de $F = \{\ket{0}\}$.

\subsubsection*{Problema do módulo}
\label{mcc:dfa:ex:módulo}
O problema do módulo $m$ consiste em verificar se uma determinada parte da
palavra ocorre um número múltiplo de $m$ vezes. Novamente para simplificar os
exemplos, usaremos o alfabeto $\{a,b\}$ e $a$ como a substring desejada. Desta
forma, podemos definir $\text{MOD}^m = (Q,\Sigma,\delta,q_0,F)$ de maneira
genérica:
\begin{description}
    \item[Q] é \{$\ket{0}$, $\ket{1}$,$\hdots$, $\ket{m-1}$\};
    \item[$\Sigma$] é \{a,b\};
    \item[$\delta$] é dado pelas seguintes matrizes de dimensões $m \times m$
        \begin{equation}
            A = \sum_{i=1}^{m} \ketbra{i\%m}{i-1)}
        \end{equation}
        , onde "\%"\ é a operação módulo;\\
        \begin{equation}
            B = I;
        \end{equation}
       , onde $I$ é a matriz identidade;
    \item[$q_0$] é $\ket{0}$;
    \item[F] é $\{\ket{0}\}$.
\end{description}
A matriz $B$ nos garante que não importa quantos $b$'s a palavra tenha o estado
continuará inalterado. Já a matriz $A$ nos garante a contagem de $a$'s ao
alternar entre os estados de forma cíclica, ou seja, alternando entre $\ket{0},
\ket{1}, \hdots, \ket{m-2}, \ket{m-1}, \ket{0}$ e assim por diante até o fim da
entrada. Assim o estado do autômato será $\ket{i}$ quando a quantidade de $a$'s
módulo $m$ lidos for igual a $i$. E a palavra será aceita se o estado final for
$\ket{0}$, ou seja, quando a quantidade de $a$'s módulo $m$ for igual a 0, caso
contrário a palavra é rejeitada.

Como exemplo, vamos considerar $\text{MOD}^4 = (Q,\Sigma,\delta,q_0,F)$:
\begin{equation}
\begin{split}
    A = \sum_{i=1}^{m} \ketbra{i\%m}{i-1} =
    \begin{bmatrix}
        0 & 0 & 0 & 1 \\
        1 & 0 & 0 & 0 \\
        0 & 1 & 0 & 0 \\
        0 & 0 & 1 & 0 \\
    \end{bmatrix}
    ;\\
    B = I;
\end{split}
\end{equation}

A computação de $aaaaab$ é dada por:
\begin{equation}
    P_\text{acc}AAAAAB q_0,
\end{equation}
onde $P_\text{acc}$ é dado por:
\begin{equation}
    P_\text{acc} = \sum_{i\in F} \ketbra{i}{i} = \ketbra{0}{0};
\end{equation}
e computação é:
\begin{equation}
\begin{split}
    P_\text{acc}AAAAAB q_0 = P_\text{acc}A^5B \ket{0} \\
    = P_\text{acc}A^5 \ket{0} = P_\text{acc}A^4 \ket{1} \\
    = P_\text{acc}A^3 \ket{2} = P_\text{acc}A^2 \ket{3} \\
    = P_\text{acc}A \ket{0} = \ketbra{0}{0} \ket{1} = 0,
\end{split}
\end{equation}
como o resultado é 0, $aaaaab$ não é aceita por $M$.

Vamos agora considerar $bbbababaa$ a computação seria:
\begin{equation}
\begin{split}
    P_\text{acc}AABABABBB q_0 = P_\text{acc}AABABABBB \ket{0} \\
    = P_\text{acc}AABABABB \ket{0} = P_\text{acc}AABABAB \ket{0} \\
    = P_\text{acc}AABABA \ket{0} = P_\text{acc}AABAB \ket{1} \\
    = P_\text{acc}AABA \ket{1} = P_\text{acc}AAB \ket{2} \\
    = P_\text{acc}AA \ket{2} = P_\text{acc}A \ket{3} \\
    = \ketbra{0}{0} \ket{0} = 1,
\end{split}
\end{equation}
como temos o resultado 1, $bbbababaa$ é aceita por $M$.

Feita a revisão dos autômatos, seu funcionamento e a apresentação da notação de
\textit{Dirac} para aproximação dos AF's para os modelos quânticos, será feita
uma revisão sobre a álgebra linear necessária no estudo da computação quântica.

