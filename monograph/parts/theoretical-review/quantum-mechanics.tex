\section{Postulados da Mecânica Quântica}
\label{sec:quantummec}

A computação quântica se alicerça na mecânica quântica para computar, portanto
é fundamental o entendimento de seus conceitos mais básicos, antes de partir à
computação em si.

A mecânica quântica, por si, não descreve as leis aos quais os sistemas físicos
estão sujeitos, porém, disponibiliza o aparato matemático e conceitual para
desenvolvimento dessas leis.

\textit{Nielsen} e \textit{Chuang} \cite{nielsen2010quantum} enunciam os
postulados da mecânica quântica, eles são referentes a: Espaço de um Sistema
(\cref{subsec:estadosis}), Evolução de um Sistema (\cref{subsec:evolucaosis}),
Medida de um Sistema (\cref{subsec:medidaestado}), Sistema Composto
(\cref{subsec:sistemacomposto}).

\theoremstyle{plain}
\newtheorem{post}{Postulado}[]

%###############################################################################

\subsection{Estado de um Sistema}
\label{subsec:estadosis}

\begin{post}
    A todo sistema físico fechado existe um espaço de Hilbert associado. O
    estado do sistema pode ser completamente descrito por um vetor unitário
    pertencente a esse espaço.
\end{post}

O primeiro postulado diz sobre como a mecânica quântica descreve um sistema
físico isolado, ou seja, um sistema que não tem nenhum tipo de contato com o
ambiente externo, como troca de energia, por exemplo. Por outro lado, não
descreve qual é, ou como descobrir, qual é o espaço de Hilbert, e o vetor
unitário associado a cada sistema físico. Desta forma, é necessário um estudo
para cada sistema, a fim de descobrir o seu espaço associado e o vetor que
descreve seu estado.

Na computação quântica, trataremos de sistemas isolados - os computadores
quânticos. Desta forma, neste trabalho trataremos o estado dos modelos
apresentados como um vetor unitário pertencente a um espaço de Hilbert.

\input{parts/theoretical-review/qubit.tex}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\subsection{Evolução de um Sistema}
\label{subsec:evolucaosis}

\begin{post}
	\label{postulate:evolucao}
    A evolução no tempo do estado de um sistema quântico fechado é dada pela
    aplicação de um operador unitário.  Portanto a evolução do estado
    $\ket{\psi_0}$, no tempo 0, para o estado $\ket{\psi_1}$, no tempo 1, é
    descrita por um operador unitário $U$ tal que:
    \begin{equation}
        \ket{\psi_1} = U\ket{\psi_0},
    \end{equation}
\end{post}

A mecânica quântica, através do segundo postulado, nos diz qual é a relação
entre dois estados de um sistema quântico em diferentes instantes no tempo. E
que essa relação é a aplicação de um operador unitário. Porém, ela não diz como
é o operador que descreve a evolução em tal sistema. Ela só garante que a
evolução no tempo de um sistema é descrito desta forma.

Portanto, em um computador quântico, onde o tempo é discreto, cada variação
por unidade de tempo - um passo de computação - é descrito pela aplicação de um
operador unitário.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\subsection{Medida de um Sistema}
\label{subsec:medidaestado}

\begin{post}
	\label{postulate:medida}
    Uma medida quântica pode ser descrita por um conjunto de operadores de
    medida $\{M_m\}$ que operam sobre o estado associado ao sistema. Onde o
    índice $m$ referencia um possível resultado da medida. Portanto, dado um
    estado $\ket{\psi}$ em um sistema quântico, ao tirar a medida a
    probabilidade do resultado $m$ ocorrer é:
    \begin{equation}
        p(m) = \bra{\psi}M_m^\dagger M_m\ket{\psi},
    \end{equation}
    e o estado do sistema após a medida é dado por:
    \begin{equation}
        \frac{M_m\ket{\psi}}{\sqrt{p(m)}} =
        \frac{M_m\ket{\psi}}{\sqrt{\bra{\psi}M_m^\dagger M_m\ket{\psi}}}
        .
    \end{equation}
    Os operadores de medida satisfazem a equação de completude:
    \begin{equation}
        \sum_m M_m^\dagger M_m = I,
    \end{equation}
    de maneira equivalente, temos:
    \begin{equation}
        1 = \sum_m p(m) = \sum_m \bra{\psi}M_m^\dagger M_m\ket{\psi}.
    \end{equation}
\end{post}

Dado um sistema quântico fechado o  postulado \ref{postulate:evolucao} descreve
como é dada sua evolução. Porém em dados momentos, precisamos observar o
sistema para saber o que ocorre internamente. Essa interação faz com que o
sistema não esteja mais isolado, e portanto, a partir de agora, sua evolução
pode não ser descrita por evoluções unitárias. O postulado
\ref{postulate:medida} descreve o que ocorre quando um sistema quântico fechado
é medido.

Dado um qubit na base computacional, por exemplo, temos que seus operadores de
medida são:
\begin{align}
	M_0 = \ketbra{0}{0},\\
	M_1 = \ketbra{1}{1},
\end{align}
como tanto $M_0$ como $M_1$, são projeções hermitianas, temos que:
\begin{align}
	M_0^\dagger M_0 = M_0^2 = M_0, \\
	M_1^\dagger M_1 = M_1^2 = M_1.
\end{align}
E a equação de completude é obedecida:
\begin{equation}
	\sum_{m=0}^1 M_m^\dagger M_m = \sum_{m=0}^1 M_m = I.
\end{equation}

Supondo que em dado momento da computação, o sistema esteja no estado
$\ket{\psi} = \alpha\ket{0} + \beta\ket{1}$. A probabilidade de se medir
$\ket{0}$ e $\ket{1}$  é:
\begin{align}
	p(0) = \bra{\psi}M_0^\dagger M_0\ket{\psi} = \bra{\psi}M_0\ket{\psi} =
	\abs{\alpha}^2, \\
	p(1) = \bra{\psi}M_1^\dagger M_1\ket{\psi} = \bra{\psi}M_1\ket{\psi} =
	\abs{\beta}^2.
\end{align}
Após os respectivos resultados, o sistema se encontra no seguinte estado:
\begin{align}
	\frac{M_0\ket{\psi}}{\abs{\alpha}} = \frac{\alpha}{\abs{\alpha}} \ket{0}, \\
	\frac{M_1\ket{\psi}}{\abs{\beta}} = \frac{\beta}{\abs{\beta}} \ket{1}.
\end{align}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\subsection{Sistema Composto}
\label{subsec:sistemacomposto}
\begin{post}
    O estado de um sistema físico composto é o produto tensorial dos estados de
    Hilbert associados às componentes do sistema, ou seja, um sistema composto
    por $n$ sistemas quânticos - nos estados $\ket{\psi}_0$, $\ket{\psi}_1$,
    $\hdots$, $\ket{\psi}_{n-1}$ - tem como estado
    $\ket{\psi}_0\otimes\ket{\psi}_1\otimes\hdots\otimes\ket{\psi}_{n-1}$
\end{post}

O quarto postulado nos diz como lidar com sistemas compostos de  múltiplos
qubits. No caso de 2 qubits na base computacional, cada qubit pertence a
$\mathbb{C}^2$ e o sistema completo, pertence a $\mathbb{C}^{\otimes 2}$,
temos as seguintes possibilidades de estado:
\begin{equation}
\begin{split}
    \ket{00} = \ket{0}\otimes\ket{0} =
    \begin{bmatrix}
        1 \\ 0 \\ 0 \\ 0
    \end{bmatrix};
    \ket{01} = \ket{0}\otimes\ket{1} =
    \begin{bmatrix}
        0 \\ 1 \\ 0 \\ 0
    \end{bmatrix}; \\
    \ket{10} = \ket{1}\otimes\ket{0} =
    \begin{bmatrix}
        0 \\ 0 \\ 1 \\ 0
    \end{bmatrix};
    \ket{11} = \ket{1}\otimes\ket{1} =
    \begin{bmatrix}
        0 \\ 0 \\ 0 \\ 1
    \end{bmatrix}.
\end{split}
\end{equation}
Para definir um sistema em $\mathbb{C}^{\otimes n}$, portanto com $n$ qubits,
podemos usar a notação da base computacional, e colocar valores entre $0$ e
$2^n-1$ dentro do ket:
\begin{equation}
\ket{0} = \mqty[1\\0\\0\\\vdots\\0],
\ket{1} = \mqty[0\\1\\0\\\vdots\\0],
\ket{2} = \mqty[0\\0\\1\\\vdots\\0],
\hdots,
\ket{2^n-2} = \mqty[0\\\vdots\\0\\1\\0],
\ket{2^n-1} = \mqty[0\\\vdots\\0\\0\\1],
\end{equation}
onde neste caso, todos esses vetores tem $2^n$ linhas.

\subsubsection*{Evolução de um sistema composto}
\label{subsec:evolucaosiscomp}

Seguindo o postulado 2 (\cref{subsec:evolucaosis}), a evolução de um sistema
composto segue pela aplicação de um operador unitário. Em um sistema composto
de $n$ qubits os operadores em questão serão matrizes de $2^n\times2^n$.
Um operador de evolução para um sistema composto pode ser obtido, por exemplo,
através do produto tensorial entre os operadores de cada sistema:
\begin{equation}
    U = U_0 \otimes U_1 \otimes \hdots \otimes U_{n-1},
\end{equation}
onde $U_i$ opera com relação ao $i$-ésimo qubit. Neses casos, onde é possivel
compor a operação $U$ por meio do produto tensorial de operações de 1 qubit,
podemos facilmente operar somente com os qubits desejáveis, digamos o $i$-ésimo
e o $k$-ésimo qubit da seguinte forma:
\begin{equation}
    U = I_0 \otimes \hdots \otimes I_{i-1} \otimes U_i \otimes I_{i+1} \otimes
    \hdots \otimes I_{k-1} \otimes U_k \otimes I_{k+1} \otimes \hdots \otimes
    I_{n-1}.
\end{equation}
Outra notação que pode ser usada neste caso é:
\begin{equation}
    U = U_i \otimes U_k,
\end{equation}
onde neste caso entende-se que $U_i$ será aplicado somente com relação ao
$i$-ésimo qubit e $U_k$ será aplicado somente com relação ao $i$-ésimo qubit, e
todos os outros se mantêm inalterados.

\subsubsection*{Medida de um sistema composto}
\label{subsec:medidaestadocomp}
De forma similar a evolução de um sistema composto, os operadores de medida
também são matrizes $2^n\times2^n$. E podem ser obtidos pelo produto tensorial
dos operadores de medida dos bits desejados.
Seja um $\ket{\psi}$ um sistema composto de $n$ qubits na base computacional:
\begin{equation}
    \ket{\psi} = \alpha_0 \ket{0} + \alpha_1 \ket{1} + \hdots +
    \alpha_{2^n-1}\ket{2^n-1};
\end{equation}
os operadores para medir o seu $i$-ésimo qubit podem ser definidos por:
\begin{equation}
\begin{split}
    M_{i0} = I_0 \otimes I_1 \otimes \hdots \otimes \ketbra{0}{0}_i \otimes
    \hdots \otimes I_{2^n-1},\\
    M_{i1} = I_0 \otimes I_1 \otimes \hdots \otimes \ketbra{1}{1}_i \otimes
    \hdots \otimes I_{2^n-1},
\end{split}
\end{equation}
onde $M_{i0}$ é o operador que mede o $i$-ésimo qubit com relação ao valor $0$,
e $M_{i1}$ é o operador que mede o $i$-ésimo qubit com relação ao valor $1$.

A probabilidade de se obter o resultado $0$ no $i$-ésimo qubit é:
\begin{equation}
    p_i(0) = \bra{\psi}M_{i0}^\dagger M_{i0}\ket{\psi},
\end{equation}
e o estado do sistema após a medida é:
\begin{equation}
    \ket{\psi'} = \frac{M_{i0}\ket{\psi}}{\sqrt{p_i(0)}}.
\end{equation}

De maneira análoga, no caso do resultado $1$ temos a seguinte probabilidade:
\begin{equation}
    p_i(1) = \bra{\psi}M_{i1}^\dagger M_{i1}\ket{\psi},
\end{equation}
e o estado do sistema após a medida é:
\begin{equation}
    \ket{\psi'} = \frac{M_{i1}\ket{\psi}}{\sqrt{p_i(1)}}.
\end{equation}

\subsubsection*{Estados emaranhados}
\label{subsec:estadosemaranhados}
É possível que um sistema composto não possa ser decomposto num produto
tensorial de seus qubits, neste caso o sistema está em um estado emaranhado. A
principal característica de um estado emaranhado é que existem qubits
correlacionados e a leitura de um deles pode fazer com que um outro também
colapse.
É impossível definir o estado de cada qubit para o estado emaranhado, por
exemplo o seguinte estado:
\begin{equation}
\begin{split}
    \ket{\beta_{00}} = \frac{\ket{00}+\ket{11}}{\sqrt{2}}.
\end{split}
\end{equation}
Por não podermos decompor $\ket{\beta_{00}}$ em um produto tensorial de 2
qubits, dizemos que ele está emaranhado, e seus qubits correlacionados.  Ao
tomar a medida do primeiro qubit, podemos obter o resultado $\ket{0}_0$ com
probabilidade $\frac{1}{2}$, e resultado $\ket{1}_0$ também com probabilidade
$\frac{1}{2}$.
Supondo $\ket{0}_0$ como resultado da medida, o segundo qubit também colapsara
para $\ket{0}_1$, e o sistema por inteiro estará em $\ket{00}$.  De forma
análoga, se $\ket{1}_0$ for o resultado da medida, o segundo qubit também
colapsara para $\ket{1}_1$, e o sistema por inteiro estará em $\ket{11}$.


$\ket{\beta_{00}}$ é um dos quatro estados de \textit{Bell}, que são um conjunto
de estados emaranhados que definem uma base de estados emaranhados de 2 qubits:
\begin{equation}
\begin{split}
    \ket{\beta_{00}} = \frac{\ket{00}+\ket{11}}{\sqrt{2}};\\
    \ket{\beta_{01}} = \frac{\ket{00}-\ket{11}}{\sqrt{2}};\\
    \ket{\beta_{10}} = \frac{\ket{01}+\ket{10}}{\sqrt{2}};\\
    \ket{\beta_{11}} = \frac{\ket{01}-\ket{10}}{\sqrt{2}}.
\end{split}
\end{equation}
