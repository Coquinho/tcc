format long E
A = @(theta) [[cos(theta) -sin(theta)];[sin(theta) cos(theta)]]
q0 = [1;0]
q1 = [0;1]
mod = 11
pi
p = pi
theta = 2*p/mod

pq0 = q0*q0'
pq1 = q1*q1'

vals = []
for x = 0:90
    n = mod*x;
    vals = [vals n n+3];
endfor

res = [];
for x = vals
    ex = pq0*A(theta)**x*q0;
    ex = sqrt(ex'*ex);
    res = [res ex*ex];
endfor
res;
#r2 = [9.999999656612492E-01   2.018712120909015E-02]

format long E
pi
p = 3.1415
erropi = pi-p
theta = 2*p/mod
erroAng = abs(2*pi/mod-theta)
res2 = [];
for x = vals
    ex = pq0*A(theta)**x*q0;
    ex = sqrt(ex'*ex);
    res2 = [res2 ex*ex];
endfor
res2;

disp("Erro absoluto\n")
erro = abs(res.-res2);
[erroMax, ix] = max(erro);
erroMax = erroMax
format short
v = vals(ix)


format long E
[erroMin, ix] = min(erro(2:end));
erroMin = erroMin
format short
v = vals(ix+1)

format long E
media = 0;
for i = erro
    media += i;
endfor
media = media/size(res)(2)

dvp = 0;
for i = erro
    aux = i -media;
    dvp += aux*aux;
endfor
variancia = dvp
dvp = sqrt(dvp)

valsPar = vals(1:2:end);
erroPar = erro(1:2:end);
valsImp = vals(2:2:end);
erroImp = erro(2:2:end);

disp("Erro absoluto Par\n")
[erroMax, ix] = max(erroPar);
erroMax = erroMax
format short
v = valsPar(ix)

format long E
[erroMin, ix] = min(erroPar(2:end));
erroMin = erroMin
format short
v = valsPar(ix+1)

format long E

media = 0;
for i = erroPar
    media += i;
endfor
media = media/size(erroPar)(2)

dvp = 0;
for i = erroPar
    aux = i -media;
    dvp += aux*aux;
endfor
variancia = dvp
dvp = sqrt(dvp)

disp("Erro relativo Impar\n")
[erroMax, ix] = max(erroImp);
erroMax = erroMax
format short
v = valsImp(ix)

format long E
[erroMin, ix] = min(erroImp);
erroMin = erroMin
format short
v = valsImp(ix+1)

format long E

media = 0;
for i = erroImp
    media += i;
endfor
media = media/size(erroImp)(2)

dvp = 0;
for i = erroImp
    aux = i -media;
    dvp += aux*aux;
endfor
variancia = dvp
dvp = sqrt(dvp)
#plot(valsImp,erroImp,'g')
#plot(valsPar,erroPar,'b')
#plor(vals,erro,'r')

#plot(valsImp,erroImp,'g', valsPar,erroPar,'b',vals,erro,'r', vals,res, 'b', vals,res2, 'r')
plot(vals,res, 'xb', vals,res2, 'or')
#plot(vals,res, 'b', vals,res2, 'r')
