Título do projeto: Modelos Computacionais Quânticos
Nome do bolsista: Lucas Cavalcante de Sousa
Nome do orientador: Eduardo Inácio Duzzioni
Centro: Centro de Ciências Físicas e Matemáticas
Departamento: Departamento de Física/FSC/CFM
Grande Área: Ciências Exatas e da Terra
Área: Ciências Exatas e da Terra
Sub-área do conhecimento: Física
Nome da instituição: Universidade Federal de Santa Catarina
Palavras-chave: Informação quântica, modelos de computação quântica modelos de máquinas abstratas, computação quântica.
Resumo:

Um modelo computacional é uma forma de computar, ou seja, uma forma de solucionar um problema. Na Ciência da Computação esse estudo trouxe um entendimento sobre como se soluciona problemas com um computador e que tipo de problemas são solucionáveis com eles. Este trabalho estudou esses mesmo pontos observando versões quânticas dos modelos computacionais clássicos. Ou seja, o este trabalho tenta obter um maior entendimento sobre como se resolve problemas utilizando os efeitos da mecânica quântica para computar. O modelo estudado foi o MO1QFA, que é uma das versões quânticas dos Autômatos Finitos, um dos modelos computacionais clássicos mais simple. Como resultados dos estudos conclui-se que os autômatos finitos quânticos são modelos computacionais teóricos que utilizam menos espaço que suas versão clássica, ou seja, possuem menos estados. Eles também possuem um potencial de contagem, algo que necessita de um modelo computacional clássico mais complexo.  Más por outro lado, o resultados obtidos possuem uma incerteza atrelada, o que pode ser crucial dependendo da aplicação.

Contatos: lucks.sousa@gmail.com
