import matplotlib.pyplot as plt
import numpy as np

esp = [100, 2.02535]
error = []
with open("results.txt","r") as f:
    line = f.readline()
    while(line):
        # mod 11 = 0
        line = list(line.split())
        er = abs(float(line[2]) - esp[0])
        error.append(er)
        # mod 11 = 3
        line = list(f.readline().split())
        er = abs(float(line[2]) - esp[1])
        error.append(er)
        # read next line
        line = f.readline()
index_max = np.argmax(error)
print("er max ", error[index_max], " #",a[index_max])
plt.plot(a, error)
plt.show()


errorPar = []
errorImp = []
valPar = []
valImp = []

for i,val in enumerate(error):
    if i % 2 == 0:
        errorPar.append[val]
        valPar.append[a[i]]
    else:
        errorImp.append[val]
        valImp.append[a[i]]


media = 0
# [index, value]
erMax = [0, 0]
# [index, value]
erMin = [0, 9999]
for i,er in enumerate(errorImp):
    media += er
    if er > erMax[1]:
        erMax = [i,er]
    elif er < erMin[1]:
        erMin = [i,er]
media /= len(errorImp)

dvp = 0
for er in errorImp:
    aux = er - media
    dvp += aux*aux
dvp /= len(errorImp)

print("media ", media)
print("desvio padrão ", dvp)
print("erro min ", erMin[1], " #", valImp[erMin[0]])
print("erro max ", erMax[1], " #", valImp[erMax[0]])

