\chapter{Experimentação}
\label{exp}

Na \cref{sec::qaf} foram apresentados algumas aplicações do MO-1QFA's, nesta
seção pretende-se estuda-lo de maneira mais prática, a fim de verificar a
relevância de QFA nos computadores quânticos disponíveis atualmente. A
importância disso, vem do fato de que, além dos sistemas quânticos reais
possuírem erros, parte dos autômatos finitos quânticos apresentados também
funcionam de maneira probabilística para a maior parte dos problemas. Este
capítulo apresentará como mapear um MO-1QFA para o modelo circuital do IBMQ
\cref{sec::ibmq::mapeamento}, e o estudo de caso do problema do módulo 11,
assim como uma discussão sobre sua ocorrência de erros.


\section{Passos iniciais}
\label{sec::ibmq}

A IBM possui o programa IBMQ\cite{IBMQ} com o propósito de disseminar o estudo
além de incentivar a pesquisa e desenvolvimento de sistemas quânticos, nele
estão disponíveis plataformas para simulação e execução de circuitos quânticos,
onde códigos quânticos podem ser testados tanto em um simulador, quanto em
computadores quânticos reais que a IBM disponibiliza para acesso remoto. As
plataformas podem ser utilizados de duas maneiras, através do IBMQ Experience
ou através do QisKit.

O IBMQ Experience \cite{IBMQE} é uma plataforma online para o desenvolvimento e
experimentação de circuitos quânticos, que conta com uma interface gráfica do
tipo \textit{drag-and-drop} e um campo para codificar o circuito desejado.

O segundo método é utilizando a biblioteca de código aberto
QisKit\cite{QisKit}. Essa API permite a criação de circuitos quânticos através
da linguagem python, com experimentação nas diferentes plataformas
disponibilizadas por acesso remoto pela IBM.

A experimentação feita neste trabalho utiliza o QisKit e a plataforma
ibmq\_vigo de 5 qubits.

\subsection{Mapeamento do MO1QFA para o modelo circuital}
\label{sec::ibmq::mapeamento}
O sistema do IBMQ, tanto no QisKit, quanto no IBMQ Experience, utilizam o
modelo circuital\cite{MCQisKit} de computação quântica, isto é, toda a
computação é descrita por uma sequência de portas quânticas, onde cada porta
representa uma matriz de rotação no qubit especificado.

Todos os modelos apresentados na \cref{sec::qaf} possuem somente dois estados,
portanto todos eles utilizam 1 qubit, onde o $\ket{0}$ representa um dos
estados e o $\ket{1}$ o outro; e cada transição dos autômatos devem ser
mapeadas para uma porta lógica.

Para descrever a computação de uma determinada entrada em um determinado
autômato no QisKit, devemos descrever a sequência de transições tomadas pelo
autômato, ou seja, a sequência de rotações executadas.

O QisKit nos possibilita realizar tais rotações por mais de uma porta, aqui
será utilizada a porta $Ry(\theta$)\cite{QisKitRy}, que é uma porta de 1
qubit, que permite realizar uma rotação do ângulo $\theta$ ao redor do eixo $y$.
Podemos utilizá-la para realizar todas as transições nos autômatos descritos na
\cref{sec::qaf}, com um único adendo, pela forma como as portas de um qubit
funcionam no QisKit\cite{U3QisKit} e a matriz utilizada para realizar as
rotações da \cref{sec::qaf} o ângulo $\theta$ especificado no QisKit deve ser o
dobro do ângulo que realmente queremos de acordo com a descrição do autômato.
Isto se deve pois a porta $Ry(\theta)$ é mapeada para seguinte
matriz\cite{U3QisKit}:
\begin{equation}
\begin{bmatrix}
  cos(\frac{\theta_1}{2}) & -sin(\frac{\theta_1}{2})\\
  sin(\frac{\theta_1}{2}) & cos(\frac{\theta_1}{2}).
\end{bmatrix},
\end{equation}
Ao iguala-la com a matriz especificada na \cref{sec::qaf} temoS:
\begin{equation}
\begin{bmatrix}
  cos(\frac{\theta_1}{2}) & -sin(\frac{\theta_1}{2})\\
  sin(\frac{\theta_1}{2}) & cos(\frac{\theta_1}{2})
\end{bmatrix} =
\begin{bmatrix}
  cos(\theta_2) & -sin(\theta_2)\\
  sin(\theta_2) & cos(\theta_2)
\end{bmatrix},
\end{equation}
obtemos que $\theta_1 = 2\theta_2$.

Dessa forma o mapeamento do MO1QFA para o modelo circuital do QisKit se dá por
uma sequência de portas $Ry$ que descrevem as rotações aplicadas pelas
transições tomadas para execução de uma entrada no autômato. Mais precisamente
serão utilizadas portas $Ry(2\theta_2)$, onde $\theta_2$ é o ângulo
especificado na \cref{sec::qaf}.

\section{Problema do módulo 11}
\label{sec::mod11}
Na \cref{mo1qfa:mod} é mostrado de forma genérica como definir um MO1QFA para
resolver o problema do módulo para um primo $p$:
\begin{description}
    \item $M$ é dado pela quíntupla $(Q,\Sigma,\delta,q,F)$:
    \item[Q] é \{$\ket{0}$,$\ket{1}$\} | $\ket{0}$ e $\ket{1}$ são
        ortonormais;
    \item[$\Sigma$] é \{$a$\};
    \item[$\delta$] é dado pela matriz:
        \begin{equation}
            A =
            \begin{bmatrix}
                \cos\frac{2\pi}{p} & -\sin\frac{2\pi}{p}\\
                \sin\frac{2\pi}{p} & \cos\frac{2\pi}{p}
            \end{bmatrix};
        \end{equation}
    \item[q] é $\ket{0}$;
    \item[F] é $\{\ket{0}\}$.

\end{description}

De forma mais ilustrativa tem-se o diagrama da \cref{exp:modp}.
\begin{figure}[h]
  \centering % centers the figure
  \begin{tikzpicture}[->, >=stealth, node distance=3.5cm, every state/.style={thick, fill=gray!10},initial text=$ $]
    \label{fig::ex::mod}
    \node[state, initial, accepting] (0) {$\ket{0}$};
    \node[state, right of=0] (1) {$\ket{1}$};
    \draw
    (0) edge[loop above] node[text width=1.4cm,align=left]
    {$a$,$cos(\frac{2\pi}{p})$} (0)
    (0) edge[bend right, below] node[text width=1.4cm,align=center]
    {$a$,$sin(\frac{2\pi}{p})$} (1)
    (1) edge[loop above, above right] node[text width=1.4cm,align=center]
    {$a$,$cos(\frac{2\pi}{p})$} (1)
    (1) edge[bend right, above] node[text width=1.4cm,align=center]
    {$a$,$-sin(\frac{2\pi}{p})$} (0);
  \end{tikzpicture}

  \caption{Autômato que reconhece uma palavra com número de a's múltiplo de $p$.}
  \label{exp:modp}
\end{figure}

Neste caso de estudo, será explorado o erro que ocorre durante a execução do
MO1QFA que resolve módulo 11 sendo executado pelo QisKit.

\subsection{Autômato e mapeamento}
\label{sec::mod11::autômato}
Um MO1QFA que resolve módulo para o primo 11 é definido pela quíntupla
$(Q,\Sigma,\delta,q,F)$:
\begin{description}
    \item[Q] é \{$\ket{0}$,$\ket{1}$\} | $\ket{0}$ e $\ket{1}$ são
        ortonormais;
    \item[$\Sigma$] é \{$a$\};
    \item[$\delta$] é dado pela matriz:
        \begin{equation}
            A =
            \begin{bmatrix}
                cos\frac{2\pi}{11} & -sin\frac{2\pi}{11}\\
                sin\frac{2\pi}{11} & cos\frac{2\pi}{11}
            \end{bmatrix};
        \end{equation}
    \item[q] é $\ket{0}$;
    \item[F] é $\{\ket{0}\}$.

\end{description}

E pode ser visualizado pelo seguinte diagrama de estados:
\begin{figure}[h]
  \centering % centers the figure
  \begin{tikzpicture}[->, >=stealth, node distance=3.5cm, every state/.style={thick, fill=gray!10},initial text=$ $]
    \label{fig::ex::mod}
    \node[state, initial, accepting] (0) {$\ket{0}$};
    \node[state, right of=0] (1) {$\ket{1}$};
    \draw
    (0) edge[loop above] node[text width=1.4cm,align=left]
    {$a$,$cos(\frac{2\pi}{11})$} (0)
    (0) edge[bend right, below] node[text width=1.4cm,align=center]
    {$a$,$sin(\frac{2\pi}{11})$} (1)
    (1) edge[loop above, above right] node[text width=1.4cm,align=center]
    {$a$,$cos(\frac{2\pi}{11})$} (1)
    (1) edge[bend right, above] node[text width=1.4cm,align=center]
    {$a$,$-sin(\frac{2\pi}{11})$} (0);
  \end{tikzpicture}

  \caption{Autômato que reconhece uma palavra com número de a's múltiplo de 11.}
\end{figure}

Como o ângulo em $A$ é $\frac{2\pi}{11}$ ao mapear para a porta $Ry$ do QisKit
será utilizado o seguinte valor $\theta$:
\begin{equation}
  \theta = 2 \times \frac{2\pi}{11} = \frac{4\pi}{11}.
\end{equation}

Portanto, os testes feitos tiveram a quantidade de portas $Ry(\frac{4\pi}{11})$
igual ao números de $a$'s da entrada, por exemplo, se a entrada tiver $k$
$a$'s, serão incluídos $k$ portas $Ry(\frac{4\pi}{11})$ e em seguida a porta de
medida. Mais informações sobre a implementação e o uso do QisKit podem ser
encontradas no \cref{appendice:code}.

A probabilidade teórica do autômato aceitar uma dada entrada $w$ é dada por:
\begin{equation}
\begin{split}
  f(w) = \norm{P_{acc}U_wq_0}^2 \\
  = \norm{\ketbra{0}{0}A^{|w|}\ket{0}}^2.\\
\end{split}
\end{equation}
Dado a natureza de $A$, $A^{|w|}$ é:
\begin{equation}
A^{|w|} =
\begin{bmatrix}
  \cos\left(\frac{2\pi}{11}|w|\right) & -\sin\left(\frac{2\pi}{11}|w|\right)\\
  \sin\left(\frac{2\pi}{11}|w|\right) & \cos\left(\frac{2\pi}{11}|w|\right)
\end{bmatrix},
\end{equation}
portanto:
\begin{equation}
\begin{split}
  f(w) = \norm{\ketbra{0}{0}A^{|w|}\ket{0}}^2\\
  = \norm{\begin{bmatrix}
           cos\left(\frac{2\pi}{11}|w|\right) & -sin\left(\frac{2\pi}{11}|w|\right)\\
          \end{bmatrix}
  \ket{0}}^2\\
  = \norm{cos\left(\frac{2\pi}{11}|w|\right)}^2\\
  = cos^2\left(\frac{2\pi}{11}|w|\right),
\end{split}
\label{exp:mod11:eq:probteorica}
\end{equation}
onde $|w|$ é o tamanho da palavra $w$.

\subsection{Casos estudados, resultados e discussão}
\label{sec::mod11::casoestudo}
Inicialmente os casos de teste estudados foram as instâncias com tamanho entre
0 e 1000 que são congruentes a 0 ou a 3 módulo 11, ou seja, foram testados 182
entradas diferentes, cada uma com 8192 execuções no ibmq\_vigo.

Teoricamente(\cref{exp:mod11:eq:probteorica}), é esperado que os valores
congruentes a 0 módulo 11 tivessem 100\% de probabilidade de serem aceitos e os
valores congruentes a 3 módulo 11 tivessem 2,0253\% de probabilidade de serem
aceitos.

Nas figuras (\cref{sim:mod11:0t01000pontos} a
\cref{sim:mod11:0t01000pontosImpar}) podemos observar e comparar os valores
reais com os obtidos. Os valores do erro absoluto(módulo da diferença entre a
probabilidade esperada e a probabilidade real obtida nos experimentos) obtidos
podem ser visualizados nas tabelas (\cref{sim:mod11:0t01000pontos:tabela} a
\cref{sim:mod11:0t01000pontosImpar:tabela}).

\begin{figure}[h]
\centering
\includegraphics[scale= 0.2]{img/sim-mod11/mod11-0to1000pontos.png}
\caption{Comparação das probabilidades esperadas, em vermelho, e a probabilidade obtida, em azul. Os valores de cima são referentes aos tamanhos congruentes a 0 módulo 11, e os de baixo aos tamanhos congruentes a 3 módulo 11.}
\label{sim:mod11:0t01000pontos}
\end{figure}


\begin{table}[h]
  \centering
\begin{tabular}{|l|l|l|l|l}
\cline{1-4}
  Erro máximo   & 9.900918211974883 & Tamanho da entrada & 520\\ \cline{1-4}
  Erro mínimo   & 5.2978515625 & Tamanho da entrada & 187\\\cline{1-4}
  \multicolumn{2}{|l|}{Média} & \multicolumn{2}{l|}{7.578928049153639} &\\\cline{1-4}
  \multicolumn{2}{|l|}{Variância} & \multicolumn{2}{l|}{1.205517567315090} &  \\ \cline{1-4}
  \multicolumn{2}{|l|}{Desvio padrão} & \multicolumn{2}{l|}{1.097960640148403} &  \\ \cline{1-4}
\end{tabular}
  \caption{Resumo dos valores de erro absoluto obtidos.}
\label{sim:mod11:0t01000pontos:tabela}
\end{table}

Na \cref{sim:mod11:0t01000pontos} e \cref{sim:mod11:0t01000pontos:tabela}
podemos ver todos os casos analisados, o valor de erro é alto (com uma média de
aproximadamente 7.579 pontos) e ocorre de maneira uniforme durante todo o
intervalo (com um desvio padrão de aproximadamente 1.98), ou seja, parece
ignorar a decoerência que deveria ocorrer na aplicação de uma quantidade alta
de portas, como é o caso de aplicar centenas de portas.

\begin{figure}[h]
\centering
\includegraphics[scale=0.2]{img/sim-mod11/mod11-0to1000pontosPar.png}
\caption{Comparação das probabilidades
esperadas, em vermelho, e a probabilidade obtida, em azul. Os valores são
referentes ás entradas com tamanho congruente a 0 módulo 11.}
\label{sim:mod11:0t01000pontosPar}
\end{figure}

\begin{table}[h]
  \centering
\begin{tabular}{|l|l|l|l|l}
\cline{1-4}
  Erro máximo   & 8.58154296875 & Tamanho da entrada & 55\\ \cline{1-4}
  Erro mínimo   & 5.2978515625 & Tamanho da entrada & 187\\\cline{1-4}
  \multicolumn{2}{|l|}{Média} & \multicolumn{2}{l|}{6.664502489697802} &\\\cline{1-4}
  \multicolumn{2}{|l|}{Variância} & \multicolumn{2}{l|}{0.3123444256150856} &  \\ \cline{1-4}
  \multicolumn{2}{|l|}{Desvio padrão} & \multicolumn{2}{l|}{0.5588778270920091} &  \\ \cline{1-4}
\end{tabular}
  \caption{Resumo dos valores de erro absoluto obtidos para entradas com tamanho
  congruente a 0 módulo 11.}
\label{sim:mod11:0t01000pontosPar:tabela}
\end{table}

Na \cref{sim:mod11:0t01000pontosPar} e \cref{sim:mod11:0t01000pontosPar:tabela}
podemos ver os casos congruentes a 0. Aqui o erro ainda é alto, porém
ligeiramente menor (com uma média de aproximadamente 6.665 pontos, quase um
ponto a menos que a média geral) e ocorre de maneira uniforme (com um desvio
padrão de aproximadamente 0.559).

\begin{figure}[h]
\centering
\includegraphics[scale=0.2]{img/sim-mod11/mod11-0to1000pontosImpar.png}
\caption{Comparação das probabilidades
esperadas, em vermelho, e a probabilidade obtida, em azul. Os valores são
referentes às entradas com tamanho congruente a 3 módulo 11.}
\label{sim:mod11:0t01000pontosImpar}
\end{figure}

\begin{table}[h]
  \centering
\begin{tabular}{|l|l|l|l|l}
\cline{1-4}
  Erro máximo   & 9.900918211974883 & Tamanho da entrada & 520\\ \cline{1-4}
  Erro mínimo   & 7.056679930724883 & Tamanho da entrada & 267\\\cline{1-4}
  \multicolumn{2}{|l|}{Média} & \multicolumn{2}{l|}{8.493353608609491} &\\\cline{1-4}
  \multicolumn{2}{|l|}{Variância} & \multicolumn{2}{l|}{0.4263425014428133} &  \\ \cline{1-4}
  \multicolumn{2}{|l|}{Desvio padrão} & \multicolumn{2}{l|}{0.6529490802833046} &  \\ \cline{1-4}
\end{tabular}
  \caption{Resumo dos valores de erro absoluto obtidos para entradas com tamanho
  congruente a 3 módulo 11.}
\label{sim:mod11:0t01000pontosImpar:tabela}
\end{table}

Na \cref{sim:mod11:0t01000pontosImpar} e
\cref{sim:mod11:0t01000pontosImpar:tabela} podemos ver os casos congruentes a
3. Aqui o erro é mais alto que no caso dos congruentes a 0 (com uma média de
  aproximadamente 8.49 pontos, quase um ponto a mais que a média geral e quase
dois pontos a mais que no caso dos congruentes a 0) e ocorre de maneira
uniforme (com um desvio padrão de aproximadamente 0.653, ligeiramente maior que
no caso dos congruentes a 0).

Para entender os fatores que compõem o erro que ocorrem durante a computação
foi definido uma função para aproximar os valores obtidos do valor esperado,
dessa forma talvez se possa entender quais erros estão ocorrendo, e qual o
impacto de cada um.

O erro pode ser definido por 3 fatores principais: $\alpha$ (representa um erro
de amplitude), $\delta$ (representa um erro de fase) e $\beta$ (um ruído).

Com a aplicação dos erros a computação seria alterada. Sairia da computação
originalmente definida por:
\begin{equation}
  \cos^2\left(\frac{2\pi}{11}|w|\right),
\end{equation}
para a influênciada pelo erro:
\begin{equation}
  \alpha \cos^2\left(\frac{2\pi}{11}|w|+\delta\right) + \beta.
\end{equation}

A função de aproximação definida, focou nos valores de $\alpha$ e $\beta$, já
que, nesse caso, erros de fase não seriam relevantes. Foram obtidos, com
testes empíricos, os valores de 0.85 e 8.5 referentes respectivamente a
$\alpha$ e $\beta$:
\begin{equation}
  0.85 \times cos^2\left(\frac{2\pi}{11}|w|\right) + 8.5,
\end{equation}

O erro entre os valores obtidos e a função aproximada é pequeno, como
apresentado na \cref{sim:mod11:fit}.

\begin{figure}[h]
\centering
\includegraphics[scale=0.2]{img/sim-mod11/fitting.png}
\label{exemp::mod11::0t01000pontosImpar}
\caption{Comparação das probabilidades esperada após a função de
aproximação, em vermelho, e a probabilidade obtida, em azul.}
\end{figure}

\begin{table}[h]
  \centering
\begin{tabular}{|c|c|}
\hline
Média     & 0.5329689393703508 \\
\hline
Variância & 0.1438948867048030 \\
\hline
Desvio padrão    & 0.3793347950093729 \\
\hline
\end{tabular}
  \caption{Resumo dos valores de erro obtidos após a função de aproximação.}
  \label{sim:mod11:fit}
\end{table}

O que indica que os valores $\alpha$ e $\beta$ obtidos são próximos aos reais
valores de erro.

Percebe-se que valores melhores para $\alpha$ e $\beta$ podem ser obtidos
a partir dos erros já apresentados nas \cref{sim:mod11:0t01000pontos:tabela} a
\cref{sim:mod11:0t01000pontosImpar:tabela}. $\beta$ pode ser definido como o
erro médio dos valores com tamanho congruentes a 3 módulo 11, o que ``levanta''
a função ao ser somado. E $\alpha$ pode ser definido pela seguinte equação:
\begin{equation}
\alpha = (100 - E_0 - \beta)/100 \approx 0.848,
\end{equation}
onde $E_0$ representa o erro médio dos valores com tamanho congruente 0 módulo
11.

Como os valores de ótimos são bem próximos dos valores obtidos empiricamente,
o erro médio, variância e desvio padrão não são muito alterados, com
diferenças somente a partir da segunda casa
decimal(\cref{sim:mod11:fit:optimo}).

\begin{table}[h]
  \centering
\begin{tabular}{|c|c|}
\hline
Média     & 0.51903759262535 \\
\hline
Variância & 0.1470678913781397 \\
\hline
Desvio padrão & 0.3834943172696822 \\
\hline
\end{tabular}
  \caption{Resumo dos valores de erro obtidos após a função de aproximação com
  valores $\alpha$ e $\beta$ ótimos.}
  \label{sim:mod11:fit:optimo}
\end{table}

