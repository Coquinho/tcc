source: https://perso.esiee.fr/~mustafan/TechnicalWritings/Complexityslides/lec14.pdf

### One-sided error
    + The machine may err only in one direction i.e. either on ‘yes’ instances
      or on ‘no’ instances.
    + Either false positive or false negative, not both.
### Two-sided error
    + The machine may err in both directions i.e. it may result a ‘yes’
      instance as a ‘no’ instance and vice versa.
    + Both false positive and false negative can occur.
### Zero-sided error
    +The algorithm never provides a wrong answer.
    +But sometimes it returns ‘don’t know’
