Olá, Sou Lucas Calcante de Sousa e orientado pelo Prof. Eduardo Inácio Duzzioni
pesquisei sobre Modelos Computacionais Quânticos.
Um modelo computacional é uma forma de computar, ou seja, uma forma de
solucionar um problema.
Na Ciência da Computação o estudo dos modelos computacionais trouxe um
entendimento sobre como se soluciona problemas com um computador e que tipo de
problemas são solucionáveis com eles.
Neste estudo o objetivo foi obter um maior entendimento sobre esses mesmos
pontos, porém observando versões quânticas dos modelos computacionais clássicos.
Quando digo clássico, me refiro aos modelos computacionais baseados na física
clássica, como, os computadores e smartphones. E quando digo quântico, me
refiro a contrapartida baseadas nos efeitos físicos da mecânica quântica.
Ou seja, o objetivo desse trabalho foi obter um maior entendimento sobre como
se resolve problemas utilizando os efeitos da mecânica quântica para computar.
O modelo estudado foi o MO1QFA, que é uma das versões quânticas dos Autômatos
Finitos, um dos modelos computacionais mais simple.

Os Autômatos Finitos reconhecem uma linguagem, ou seja, dada uma palavra de
entrada, a saída dessa computação é dizer se a palavra pertence ou não a
linguagem do autômato.

Por exemplo, aqui temos um autômato que reconhece palavras constituídas de a's
e b's que possuem uma quantidade par de a's, sem a's consecutivos, ou seja,
entre dois a's sempre existe ao menos um b.

Como esse autômato funciona? As bolas representam estados, indicam em que ponto
está a computação; as flechas são as transições, elas indicam uma mudança de
estado dado um gatilho. Por exemplo, em q0 se o autômato ler um b ele transita
para o próprio q0, se ele ler um a ele transita para q1.

Os estados que possuem círculos circunscritos, como q3, são estados finais,
caso a computação termine em um estado final, podemos dizer que o autômato
aceitou a palavra computada, ou seja, a palavra pertence a linguagem, caso
contrario dizemos que o autômato rejeitou a palavra, ou seja, a palavra não
pertence a linguagem.

Além disso, a seta sem origem indica em que estado a computação começa, nesse
caso q0.

Para computar aba ele passa pelo seguinte processo, inicia em q0, lê o primeiro
a transita para q1, lê um b e, portanto, transita para q2, e lê o último a e
transita para q3. Como já leu toda a palavra, ele analisa o ultimo estado, como
ele foi q3, que é um estado final, a palavra é aceita.

Agora um exemplo de erro, a palavra bbaa. O autômato inicia em q0, lê um b e
continua em q0, lê o segundo b e continua em q0, lê o primeiro a e transita
para q1, de q1 com a não existe uma transição definida e portanto o automato
retorna que rejeita a palavra.

O MO1QFA introduz efeitos quânticos ao autômato finito. Mais especificamente as
evoluções e as medidas.

A evolução de um estado quântico é descrita por uma matriz unitária, por
simplificação, aqui no diagrama as amplitudes de probabilidade descritas nas
matrizes unitárias estão descritas nas transições.  Dessa forma, dado o estado
q0 e um a na entrada, existe cosseno de menos raiz de dois pi de chance dele
transitar para q0 e seno de menos raiz de 2 pi de chance dele transitar para
q1. Permitindo que o estado esteja em uma superposição.

O outro efeito quântico é a medida, adicionada justamente para lidar com as
superposições. Durante a computação o estado atual pode ser uma superposição de
q0 e q1, porém ao fim da computação ocorre a medida e, após ela, o estado
colapsa para uma das possibilidade q0 ou q1, q0 com norma de alfa ao quadrado
de chance e q1 com norma de beta ao quadrado de chance.

Como o autômato possui dois estados, podemos utilizar somente um qubit para
representar o estado atual, e representar as transições como rotações no qubit.
Dessa forma, podemos ver a computação como um conjunto de rotações em um qubit.

Um qubit pode ser representado pela esfera de Bloch, nela podemos ver todas as
possibilidade de estado do qubit. Todo ponto na esfera pode ser definido por
essa equação.

Para o nosso autômato podemos fixar uma dimensão, e ignorar ela, obtendo,
agora, um circulo de possibilidades, onde, q0 e q1 são os eixos.

Toda vez que o autômato lê um a, uma rotação no sentido anti-horário é feita e,
toda vez que um b é lido, uma rotação no sentido horário é feito.  E como a
rotação é dada por um múltiplo irracional de pi, temos uma circunferência
densa, e rotacionar sempre no mesmo sentido, quantas voltas sejam, nunca
atingira um ponto já visitado. Dessa forma, é como se cada ponto do circulo
tivesse a informação de quantos a's foram lidos a mais que b's ou de quantos
b's forma lidos a mais que a's.

Ao final da computação é feita uma medida, que colapsa o resultado para q0 ou
q1. Caso a palavra possua a mesma quantidade de a's e b's o resultado será q0,
e a palavra é rejeitada. Caso contrario, existe uma chance maior que 0 do
resultado ser q1 e, portanto, a palavra ser aceita.

Pode ser estranho não teremos a resposta com 100% certeza. Mas por
outro lado com um MO1QFA temos a capacidade de fazer uma contagem
potencialmente infinita tendo somente os estados como memória, algo que
necessitaria um modelo computacional clássico um pouco mais complexo, um
automato de pilha.

Os autômatos finitos quânticos são modelos computacionais teóricos que utilizam
menos espaço que suas versão clássica, ou seja, possuem menos estados.

Eles também possuem um potencial de contagem, algo que necessita de um modelo
computacional clássico mais complexo.

Más por outro lado, o resultados obtidos possuem uma incerteza atrelada, o que
pode ser crucial dependendo da aplicação.
