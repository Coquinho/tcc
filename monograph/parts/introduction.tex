\chapter{Introdução}
\setcounter{chapter}{1}
\section{Computação e seus avanços}

Em 1965, \textit{Gordon. E. Moore}, cofundador da Intel, conjecturou
\cite{moore1965cramming} sobre o aumento da quantidade de transistores em um
circuito integrado (um chip) e, consequentemente, sobre o aumento da
performance dos processadores. \textit{Moore} previu que o número de
transistores em um processador dobraria anualmente. 10 anos depois, em 1975
\cite{moore1975progress}, ele revisou sua previsão, falando que o número de
transistores em um processador dobraria a cada 2 anos.

\textit{Hennessy} e \textit{Patterson} \cite{hennessy2018computer} analisaram o
desenvolvimento de performance dos processadores de 1978 à
2018 (\cref{intro:histProcessadores}) . Por meio desta análise percebemos que o
crescimento descrito por \textit{Moore} não ocorreu de fato, e mesmo com os
vários avanços tecnológicos obtidos, ele não se manteve constante.

\begin{figure}[h]
\begin{center}
\includegraphics[height=6.5cm]{img/histProcessadores.png}
\caption{Histórico de performance dos processadores de 1978 à
2018\cite{hennessy2018computer}.}
\label{intro:histProcessadores}
\end{center}
\end{figure}

Entre 1978 e 1986, o crescimento anual observado foi
de 25\% - dobrando a cada 3,5 anos. Crescimento este atribuído ao barateamento
do hardware e avanços de software, como: o avanço do microprocessador combinado
com a redução do seu preço quando produzido em massa, o uso mais difundido de
linguagens de programação (evitando programação em \textit{Assembly} e
reduzindo a necessidade de códigos direcionados à máquinas específicas,
facilitando o desenvolvimento e simplificando as camadas de abstração
necessárias durante a programação), a criação de sistemas operacionais não
proprietários (o que reduziu preço e o risco de produção arquitetural).

De 1986 à 2003, o crescimento anual de 52\% - dobrando a cada 2 anos -
foi atribuído aos avanços nas técnicas arquiteturais e organizacionais
apresentadas pelos processadores RISC(\textit{Reduced Instruction Set
Computer}) e ao desafio que foi para as outras arquiteturas se equipararem à
sua performance.

Até 2003, durante quase 17 anos, a conjectura de \textit{Moore} se mostrou próxima da
realidade. Graças a desenvolvimentos já mencionados, foi observado um
crescimento médio anual de 50\%, ou seja, dobrando a cada 2 anos. Porém, por
volta de 2003, a lei da escalabilidade de
\textit{Dennard}\cite{dennard1974design} mostrou se aproximar do fim.  Dennard
observou, em 1974, uma correlação entre densidade energética, tamanho do chip e
quantidade de transistores. Segundo \textit{Dennard}, a densidade energética de um
circuito se mantém constante mesmo com o aumento da quantidade de transistores
graças à diminuição dos mesmos. Desta forma, contanto que os transistores
usassem menos energia, eles poderiam continuar aumentando sua velocidade.  Por
volta de 2003, se mostrou inviável continuar diminuindo a densidade energética
dos circuitos e manter a exatidão do sistema. Conforme se diminui a escala dos
transistores, mais próximo se chega dos limites fundamentais físicos, o que
causa interferências nas operações devido aos efeitos quânticos do
tunelamento\cite{nielsen2010quantum}. Desta forma um ``0"\ pode virar um ``1"\ e
vice versa, mesmo que isso não seja desejado, causando tanto perda de
informação quanto erros durante o processamento.

Desta forma, para conter essa diminuição do crescimento, foram difundidos os
processadores multicores. Ainda assim, a partir de 2003 não foi observado o
crescimento seguindo a lei de \textit{Moore}: de 2003 à 2011 o crescimento foi
de 23\% ao ano - dobrando a cada 3.5 anos;

Isso se deve pela aplicação direta da lei de \textit{Amdahl}, observada por
\textit{Amdahl}\cite{amdahl1967validity} em 1967, que expõe que uma aplicação
nunca é 100\% paralelizável, além disso, ela também expõe que o crescimento de
performance de uma aplicação pelo acréscimo da quantidade de unidades de
processamento está sujeito a um limite ligado ao quanto essa aplicação é
paralelizável.

Assim de, 2011 à 2015, o crescimento foi de 12\% ao ano - dobrando a cada 8
anos. E chegando no pior até o momento, onde de 2015 à 2018 o crescimento
observado foi de 3,5\% ao ano - dobrando a cada 20 anos.

Vendo a aproximação do fim da lei de \textit{Moore} e da lei de escalabilidade
de \textit{Dennard}, aliado ao limite imposto pela lei de \textit{Amdahl},
surge a necessidade de um novo impulso à computação. Várias formas foram
propostas, como biocomputação\cite{garzon1999biomolecular} , o uso de grafeno
para fabricação dos circuitos\cite{lin2011wafer} e a computação
quântica\cite{feynman1982simulating}. O presente trabalho tratará sobre a
computação quântica\cite{nielsen2010quantum}.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\section{O porquê da Computação Quântica}

A computação quântica reúne conceitos físicos da mecânica quântica, como
superposição e entrelaçamento, à computação clássica, como nos referenciaremos
à computação atual. Graças a esses efeitos físicos, a computação quântica se
apresenta como uma forma de expandir a barreira do que é tratável atualmente
\cite{nielsen2010quantum}.

A ideia de um computador quântico foi inicialmente proposta em 1982 por
\textit{Feynman} \cite{feynman1982simulating}, após ele propor a simulação de
sistemas físicos quânticos em um computador universal - tarefa que se mostra
difícil mesmo nas máquinas atuais, devido ao crescimento exponencial requerido
para tal simulação.  Após \textit{Feynman}, vieram outros avanços importantes
na computação quântica que demonstraram prospectos para a área: em 1994,
\textit{Shor} \cite{shor1994algorithms} desenvolveu um algoritmo quântico de
tempo polinomial para fatoração e, portanto, tratável em um computador
quântico, o que trouxe grande atenção para a área, pelo risco que traz aos
sistemas de segurança atuais, como o algoritmo criptográfico RSA
\cite{rivest1978method} \cite{shor1994algorithms} e criptografia de curvas
elípticas \cite{miller1985use}\cite{roetteler2017quantum}; em 1996,
\textit{Grover} \cite{grover1996fast} desenvolveu um algoritmo de busca em base
de dados não estruturada com ganho quadrático do tamanho da estrutura com
relação a operação clássica.

Em 1985, \textit{Deutsh} \cite{deutsch1985quantum} descreveu pela primeira vez
a junção da mecânica quântica com um modelo computacional estudado na teoria da
computação clássica. \textit{Deutsh} propôs uma versão quântica da Máquina de
Turing e, baseado na tese de \textit{Church-Turing}, propôs também o princípio
de \textit{Church–Turing–Deutsch} que declara que com esse modelo universal
seria possível simular qualquer processo físico, como inicialmente idealizado
por \textit{Feynman}. A partir disso, foram criados outros modelos quânticos
das máquinas estudadas em teoria da computação.

\section{Objetivos}

Tem-se como objetivo geral o estudo de versões quânticas dos autômatos, e a sua
experimentação em um computador quântico real.

\subsubsection*{Objetivo Específico}

O presente trabalho tem como objetivos específicos:
\begin{itemize}
  \item realizar revisão sobre os modelos existentes e as relações conhecidas
    entre eles e as versões clássicas;
  \item Exemplificar problemas tratáveis com um dos modelos de autômato
    finito quântico, o MO-1QFA;
  \item Análise de erro do MO-1QFA em uma plataforma do IBMQ\cite{IBMQE}.
\end{itemize}

\section{Estrutura do Trabalho}
Este trabalho primeiramente propõe uma revisão teórica dos temas pertinentes ao
estudo dos modelos computacionais quânticos, iniciando por uma introdução aos
modelos clássicos (autômatos finitos (\cref{mcc:dfa}) e autômatos de pilha
(\cref{mcc:pda})), à álgebra linear (\cref{sec:al}) e à mecânica quântica
(\cref{sec:quantummec}), em seguida, é apresentado um histórico dos estudos dos
autômatos quânticos conhecidos(\cref{sec::qaf}), apresentação da definição e de
problemas tratáveis pelo MO-1QFA (\cref{sec::qaf}) e por fim uma experimentação
de um dos autômatos apresentados em uma plataforma quântica
real(\cref{sec::ibmq}).

